
FUNCTION_BLOCK PLS
	VAR_INPUT
		Enable : BOOL;
		IN : REAL; (*Reference value*)
		T : REAL; (*Period of Reference Value, and Window*)
		Mode : USINT; (*Position or Time Mode*)
		TON : REAL; (*On Position*)
		TOFF : REAL; (*Off Position*)
		TimerPT : REAL;
		TimerWindow : REAL;
		Hysteresis : REAL;
	END_VAR
	VAR_OUTPUT
		Q : BOOL; (*Output*)
		DataValid : BOOL; (*Error Checking *)
	END_VAR
	VAR
		startpls : BOOL;
		TOFF_time : REAL;
		TON_Timer : TON;
		zzEdge00000 : BOOL;
		TON_hyst : REAL;
		TOFF_hyst : REAL;
		TON_reset : REAL;
		TOFF_reset : REAL;
		resetrequired : BOOL;
	END_VAR
END_FUNCTION_BLOCK
