FUNCTION_BLOCK iASerialCyclic
(*
	iA Serial Communication FUNCTION Block

	Version: 		1.0

	Description:	This FUNCTION block implements the virtually everything necessary FOR serial communication 
					between devices.
	
					An independent task should be used to implement this function block, and should be tailored to
					the needs of the system, as the logic will be different for Read-only, Write-only, Read-Write,
					and Write-Read systems.
					

	Date Created:	6/17/2015
	Created BY:		MH-iA

	Date Modified:	6/17/2015
	Modified BY:	MH-iA

	Inputs:
		cmd						FUNCTION (see Commands below)				
		Enable                  Opens up the communication channel for business
		SendData				Pointer to the string of data to send
		Config					Pointer to the FB configuration of type iASerial_Config_typ
	
	Outputs:
		Status					Indicates readyness OF FUNCTION block (off, ready, busy, error, done, no input)
		ErrorID					Indicates internal level fault OF FUNCTION block
		ErrorStep				Indicates the step in which the error occured
		RecieveData          	Data recived by the read function as a String of 255 characters
	
	Commands:
		IASERIAL_CMD_NONE:			No Command
		IASERIAL_CMD_READ:			Read the input data
		IASERIAL_CMD_WRITE:			Write the output data
		
	Version History:
	1.0				First Release in FUNCTION Block Form
	
	Bugs: 
		1.  After reading successfully from the channel, the buffer isnt completely cleared. 
			This means that the read FB needs to be called a few times in order to clear out the buffer 
			AND read the next value OR RETURN frmERR_NOINPUT. This is accounted FOR in the RBUF
			state by implementing the below line of code. This rereads until the buffer is clear or there is a value
			
			ELSIF (strcmp(ADR(ReceiveData), ADR('')) = 0) THEN 
				Step := READ;
	
*)
	CASE Step OF	
		OFF: //0 -- wait for enable 	
			Status  := IASERIAL_STATUS_OFF;
			IF Enable THEN
				Step   := OPEN;
				Status := IASERIAL_STATUS_BUSY;
			ELSE
				Step   := OFF;
			END_IF	
			
		OPEN: //10 -- open the communication channel
			Status := IASERIAL_STATUS_BUSY;
			
			SerialInternal.FB.Open.enable		:= TRUE;	
			SerialInternal.FB.Open.device 	    := ADR(Config.Interface); 
			SerialInternal.FB.Open.mode 		:= ADR(Config.Mode);
			IF (ADR(Config.CommConfig) <> 0) THEN
				SerialInternal.FB.Open.config   := ADR(Config.CommConfig); // This needs to be a data type that is available to the user
			END_IF
		
			IF (SerialInternal.FB.Open.status < ERR_FUB_BUSY) AND (SerialInternal.FB.Open.status <> ERR_FUB_ENABLE_FALSE) THEN
				SerialInternal.FB.Open.enable 	:= FALSE;
				IF (SerialInternal.FB.Open.status = ERR_OK) THEN
					SerialInternal.Ident := SerialInternal.FB.Open.ident;	
					Step                 := IDLE;
				ELSE
					ErrorID		:= SerialInternal.FB.Open.status;
					ErrorStep   := OPEN;
					Step        := ERROR;
				END_IF
			END_IF
			
		IDLE: //20 -- Wait for commands
			Status := IASERIAL_STATUS_READY;
			
			IF (NOT Enable) THEN
				Step := CLOSE;
				Status := IASERIAL_STATUS_BUSY;
			ELSIF cmd = IASERIAL_CMD_READ THEN
				Step := READ;
				Status := IASERIAL_STATUS_BUSY;
			ELSIF cmd = IASERIAL_CMD_WRITE THEN
				Step := GBUF;
				Status := IASERIAL_STATUS_BUSY;
			ELSIF cmd = IASERIAL_CMD_NONE THEN
				Step := IDLE;
			END_IF
			
		READ: //30 -- Read data from serial interface
			Status := IASERIAL_STATUS_BUSY;
			
			SerialInternal.FB.Read.ident    := SerialInternal.Ident;
			SerialInternal.FB.Read.enable   := TRUE;
			
			IF (NOT Enable) THEN
				SerialInternal.FB.Read.enable := FALSE;
				Step                          := RBUF;
			ELSIF (SerialInternal.FB.Read.status < ERR_FUB_BUSY) AND (SerialInternal.FB.Read.status <> ERR_FUB_ENABLE_FALSE) THEN
				SerialInternal.FB.Read.enable			:= FALSE;
				IF (SerialInternal.FB.Read.status = ERR_OK) THEN
					SerialInternal.ReceiveData.BufAdr	:= SerialInternal.FB.Read.buffer;
					SerialInternal.ReceiveData.BufLen	:= SerialInternal.FB.Read.buflng;
					Step                                := RBUF;
				ELSIF SerialInternal.FB.Read.status = frmERR_NOINPUT THEN
					Status := IASERIAL_STATUS_NOINPUT;
					Step   := DONE;
				ELSE
					ErrorStep := READ;
					ErrorID   := SerialInternal.FB.Read.status;
					Step      := ERROR;	
				END_IF
			END_IF	
			
		RBUF: //40 -- Release the read buffer
			Status := IASERIAL_STATUS_BUSY;	
				
			//Record the recieved data	
			memset(ADR(ReceiveData), 0, SIZEOF(ReceiveData));
			memcpy(ADR(ReceiveData), SerialInternal.ReceiveData.BufAdr, SerialInternal.ReceiveData.BufLen);
					
			SerialInternal.FB.Rbuf.enable		:= TRUE;
			SerialInternal.FB.Rbuf.ident        := SerialInternal.Ident;
			SerialInternal.FB.Rbuf.buffer		:= SerialInternal.ReceiveData.BufAdr;
			SerialInternal.FB.Rbuf.buflng		:= SerialInternal.ReceiveData.BufLen;
					
			IF (SerialInternal.FB.Rbuf.status < ERR_FUB_BUSY) AND (SerialInternal.FB.Rbuf.status <> ERR_FUB_ENABLE_FALSE) THEN
				memset(SerialInternal.ReceiveData.BufAdr, 0, SerialInternal.ReceiveData.BufLen); //Reset the buffer to 0 after the data had been recieved
				SerialInternal.FB.Rbuf.enable	:= FALSE;
				IF (SerialInternal.FB.Rbuf.status = ERR_OK) THEN
					IF (NOT Enable) THEN
						Step		:= CLOSE;
					ELSIF (strcmp(ADR(ReceiveData), ADR('')) = 0) THEN //Continue reading until there is either frmERR_NOINPUT or a string is returned
						Step		:= READ;
					ELSE
						Step		:= DONE;
						Status      := IASERIAL_STATUS_DONE;
					END_IF
				ELSE
					ErrorStep := RBUF;
					ErrorID   := SerialInternal.FB.Rbuf.status;
					Step      := ERROR;	
				END_IF
			END_IF
					
		GBUF: //50 -- Requst secure buffer for write
			Status := IASERIAL_STATUS_BUSY;
			
			SerialInternal.FB.Gbuf.ident   := SerialInternal.Ident;
			SerialInternal.FB.Gbuf.enable  := TRUE;
		
			IF (NOT Enable) THEN
				SerialInternal.FB.Gbuf.enable			:= FALSE;
				Step := CLOSE;
			ELSIF (SerialInternal.FB.Gbuf.status < ERR_FUB_BUSY) AND (SerialInternal.FB.Gbuf.status <> ERR_FUB_ENABLE_FALSE) THEN
				SerialInternal.FB.Gbuf.enable			:= FALSE;
				IF (SerialInternal.FB.Gbuf.status = ERR_OK) THEN
					//Copy over the SendData information to the buffer
					SerialInternal.SendData.BufAdr		:= SerialInternal.FB.Gbuf.buffer;
					SerialInternal.SendData.BufLen		:= SerialInternal.FB.Gbuf.buflng;			
					memset(SerialInternal.SendData.BufAdr, 0, SerialInternal.SendData.BufLen);
					memcpy(SerialInternal.SendData.BufAdr, ADR(SendData), SerialInternal.SendData.BufLen); //SerialInternal.SendData.BufLen;
					Step := WRITE;
				ELSE
					ErrorStep := GBUF;
					ErrorID   := SerialInternal.FB.Gbuf.status;
					Step      := ERROR;				
				END_IF
			END_IF
		
		WRITE: //60 -- Write data to interface
			Status := IASERIAL_STATUS_BUSY;
			
			SerialInternal.FB.Write.enable				:= TRUE;
			SerialInternal.FB.Write.ident               := SerialInternal.Ident;
			SerialInternal.FB.Write.buffer				:= SerialInternal.SendData.BufAdr;
			SerialInternal.FB.Write.buflng				:= SerialInternal.SendData.BufLen;
		
			IF (NOT Enable) THEN
				SerialInternal.FB.Write.enable			:= FALSE; // Disable write
				Step := ROBUF; //Release write buffer if there is a write error				
			ELSIF (SerialInternal.FB.Write.status < ERR_FUB_BUSY) AND (SerialInternal.FB.Write.status <> ERR_FUB_ENABLE_FALSE) THEN
				SerialInternal.FB.Write.enable			:= FALSE;
				IF (SerialInternal.FB.Write.status = ERR_OK) THEN
					memset(SerialInternal.SendData.BufAdr, 0, SerialInternal.SendData.BufLen);
					Step		:= DONE;
					Status      := IASERIAL_STATUS_DONE;
				ELSIF ((SerialInternal.FB.Write.status <> ERR_OK) AND (SerialInternal.FB.Write.status <> ERR_FUB_ENABLE_FALSE)) THEN
					memset(SerialInternal.SendData.BufAdr, 0, SerialInternal.SendData.BufLen);
					Step := ROBUF; 
				END_IF
			END_IF
			
		ROBUF: //70 -- Release write buffer
			Status := IASERIAL_STATUS_BUSY;
			
			SerialInternal.FB.Robuf.enable		:= TRUE;
			SerialInternal.FB.Robuf.ident       := SerialInternal.Ident;
			SerialInternal.FB.Robuf.buffer		:= SerialInternal.SendData.BufAdr;
			SerialInternal.FB.Robuf.buflng		:= SerialInternal.SendData.BufLen;
			
			IF (SerialInternal.FB.Robuf.status < ERR_FUB_BUSY) AND (SerialInternal.FB.Robuf.status <> ERR_FUB_ENABLE_FALSE) THEN
				SerialInternal.FB.Robuf.enable	:= FALSE;		
				IF (SerialInternal.FB.Robuf.status = ERR_OK) THEN
					IF (NOT Enable) THEN
						Step := CLOSE;
					ELSE
						Step := GBUF; //Try again
					END_IF
				ELSE
					ErrorStep := ROBUF;
					ErrorID   := SerialInternal.FB.Robuf.status;
					Step      := ERROR;
				END_IF
			END_IF
			
		CLOSE: //80 -- Close the connection. Executed on negative edge of enable
			Status := IASERIAL_STATUS_BUSY;
			
			SerialInternal.FB.Close.enable		:= TRUE;	
			SerialInternal.FB.Close.ident	    := SerialInternal.Ident;
		
			IF (SerialInternal.FB.Close.status < ERR_FUB_BUSY) AND (SerialInternal.FB.Close.status <> ERR_FUB_ENABLE_FALSE) THEN
				SerialInternal.FB.Close.enable 	:= FALSE;
				IF (SerialInternal.FB.Close.status = ERR_OK) THEN
					Step := OFF;
					cmd  := IASERIAL_CMD_NONE;
				ELSE
					ErrorStep := CLOSE;
					ErrorID   := SerialInternal.FB.Close.status;
					Step      := ERROR;
				END_IF
			END_IF
			
		DONE: //90 -- State to keep the "DONE" or "NOINPUT" Status alive until IDLE state is reached
			cmd := IASERIAL_CMD_NONE;
			IF (NOT Enable) THEN
				Step := CLOSE;
			ELSE
				Step := IDLE;	
			END_IF
			
		ERROR: //255 -- Error handling
			Status := IASERIAL_STATUS_ERROR;
			cmd    := IASERIAL_CMD_NONE;
			IF (NOT Enable) THEN
				Step := OFF;
				ErrorStep := 0;
				ErrorID   := 0;
			ELSE
				Step := ERROR;	
			END_IF	
				
	END_CASE	
	
	//Call FUBs
	SerialInternal.FB.Open();
	SerialInternal.FB.Read();
	SerialInternal.FB.Rbuf();
	SerialInternal.FB.Gbuf();
	SerialInternal.FB.Write();
	SerialInternal.FB.Robuf();
	SerialInternal.FB.Close();
	
	IF (Step <> StepTrace[0]) THEN 
		FOR i := 99 TO 1 BY (-1) DO
			StepTrace[i] := StepTrace[i-1];
		END_FOR;
		StepTrace[0] := Step;
	END_IF
	
END_FUNCTION_BLOCK
