(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Library: iASerial
 * File: iASerial.typ
 * Author: mhanson
 * Created: June 12, 2015
 ********************************************************************
 * Data types of library iASerial
 ********************************************************************)

TYPE
	SerialInternal_FB_typ : 	STRUCT 
		Open : FRM_xopen;
		Read : FRM_read;
		Rbuf : FRM_rbuf;
		Gbuf : FRM_gbuf;
		Write : FRM_write;
		Robuf : FRM_robuf;
		Close : FRM_close;
	END_STRUCT;
	SerialInternal_RecData_typ : 	STRUCT 
		String : STRING[255];
		BufAdr : UDINT;
		BufLen : UINT;
	END_STRUCT;
	SerialInternal_SendData_typ : 	STRUCT 
		String : STRING[255];
		BufAdr : UDINT;
		BufLen : UINT;
	END_STRUCT;
	SerialInternal_typ : 	STRUCT 
		State : USINT;
		FB : SerialInternal_FB_typ;
		ReceiveData : SerialInternal_RecData_typ;
		SendData : SerialInternal_SendData_typ;
		Ident : UDINT;
		ErrorID : UINT;
	END_STRUCT;
	iASerial_Config_typ : 	STRUCT 
		CommConfig : XOPENCONFIG; (*Advanced communication parameters. See FRM_xopen() for help*)
		Interface : STRING[20]; (*Name of the serial communication interface*)
		Mode : STRING[100]; (*String to specify the the communication configuration. See FRM_xopen() for help*)
	END_STRUCT;
END_TYPE
