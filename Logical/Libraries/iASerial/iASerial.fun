(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Library: iASerial
 * File: iASerial.fun
 * Author: mhanson
 * Created: June 12, 2015
 ********************************************************************
 * Functions and function blocks of library iASerial
 ********************************************************************)

FUNCTION_BLOCK iASerialCyclic
	VAR_INPUT
		Enable : BOOL; (*Enable the function block *)
		cmd : UINT; (*Command IASERIAL_CMD_XXX*)
		SendData : REFERENCE TO STRING[255]; (*Data to send*)
		Config : REFERENCE TO iASerial_Config_typ; (*Interface configuration *)
	END_VAR
	VAR_OUTPUT
		ReceiveData : STRING[255];
		ErrorID : UINT;
		ErrorStep : UINT;
		Status : UINT; (*Status of the FB IASERIAL_STATUS_XXX*)
	END_VAR
	VAR
		Step : UINT;
		StepTrace : ARRAY[0..99] OF UINT;
		SerialInternal : SerialInternal_typ;
	END_VAR
	VAR CONSTANT
		OFF : UINT := 0;
		OPEN : UINT := 10;
		IDLE : UINT := 20;
		READ : UINT := 30;
		RBUF : UINT := 40;
		GBUF : UINT := 50;
		WRITE : UINT := 60;
		ROBUF : UINT := 70;
		CLOSE : UINT := 80;
		DONE : UINT := 90;
		ERROR : UINT := 255;
	END_VAR
	VAR
		i : UINT;
	END_VAR
END_FUNCTION_BLOCK
