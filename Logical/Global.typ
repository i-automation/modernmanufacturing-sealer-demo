(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * File: Global.typ
 * Author: sswindells
 * Created: December 08, 2011
 ********************************************************************
 * Global data types of project TempCtrl
 ********************************************************************)

TYPE
	DateTimeStr_typ : 	STRUCT 
		year : STRING[5];
		month : STRING[5];
		day : STRING[5];
		wday : STRING[5];
		hour : STRING[5];
		minute : STRING[5];
		second : STRING[5];
		millisec : STRING[5];
		microsec : STRING[5];
		full : STRING[40];
	END_STRUCT;
	DateTime_typ : 	STRUCT 
		num : DTStructure;
		str : DateTimeStr_typ;
	END_STRUCT;
	SysDump_typ : 	STRUCT 
		cmd : BOOL;
		inProgress : BOOL;
		inError : BOOL;
		complete : BOOL;
	END_STRUCT;
	Modes_enum : 
		(
		MODE_INIT,
		MODE_OFF,
		MODE_MANUAL,
		MODE_AUTO,
		MODE_ERROR_STOP
		);
END_TYPE

(*Temperature control datatypes*)

TYPE
	TempCtrl_Zone_typ : 	STRUCT 
		Enabled : BOOL;
		SetPoint : REAL;
	END_STRUCT;
	TempCtrl_typ : 	STRUCT 
		ToleranceHigh : REAL;
		ToleranceLow : REAL;
		Zone : ARRAY[0..3]OF TempCtrl_Zone_typ;
	END_STRUCT;
END_TYPE

(*Recipe datatypes*)

TYPE
	Recipe_typ : 	STRUCT 
		Name : STRING[20];
		TemperatureZone : ARRAY[0..11]OF Recipe_TemperatureZone_typ;
		RunSpeed : REAL;
	END_STRUCT;
	Recipe_TemperatureZone_typ : 	STRUCT 
		Enabled : BOOL;
		SetPoint : REAL;
		ToleranceLow : REAL;
		ToleranceHigh : REAL;
	END_STRUCT;
END_TYPE

(*Alarm manager datatypes*)

TYPE
	AlarmMgr_typ : 	STRUCT 
		cmd : UINT;
		Init : BOOL;
		Banner : WSTRING[MAX_ALARM_LENGTH];
		ResetInProgress : BOOL;
		MessageState : UINT;
		PowerOnCycles : UDINT;
		OperatingHoursPP : UDINT;
		BatteryStatusCPU : USINT;
		TemperatureCPU : UINT;
		TemperatureENV : UINT;
		EStopOK : BOOL;
		ErrCritical : BOOL;
		ErrMMHandled : BOOL;
		ErrFatal : BOOL;
		SafetyCPUStat : UINT;
		SafetyInit : BOOL;
		RestartRequired : BOOL;
	END_STRUCT;
END_TYPE

(*System Settings datatypes*)

TYPE
	SystemSettings_TempZone_typ : 	STRUCT 
		Name : STRING[20];
		ControllerSettings : lcrtemp_set_typ;
	END_STRUCT;
	SystemSettings_SMTP_typ : 	STRUCT 
		MessageData : ARRAY[0..6]OF SystemSettings_Message_typ;
		RecipientAddresses : ARRAY[0..4]OF STRING[30]; (*Email Recipient Addresses indiviually*)
		Password : STRING[50]; (*Password of the sending email address*)
		Username : STRING[50]; (*Username of the sending address*)
		ModelName : STRING[30]; (*OEM machine model*)
		MachineName : STRING[30]; (*Unique Machine Identifier used in subject*)
		SenderAdr : STRING[50]; (*Name that appears in the "from" line of the email*)
		IPParts : ARRAY[0..3]OF USINT;
		TimersElapsed : USINT;
		SetInterval : TIME;
		ElapsedRuntime : TIME;
	END_STRUCT;
	SystemSettings_Message_typ : 	STRUCT 
		Enable : BOOL;
		Subject : STRING[50]; (*Subject of the email*)
		EnableWarning : BOOL;
		Text : STRING[250]; (*Body of the email*)
		WarningTime : TIME;
		FollowUp : TIME;
		SendInterval : TIME;
	END_STRUCT;
	SystemSettings_Motor_typ : 	STRUCT 
		Type : UINT;
		EncoderDirection : UINT;
		EncoderCheckBypass : UINT;
		ScalingEncoderCounts : UDINT;
		ScalingMotorRevs : UDINT;
		TemperatureModelDisable : UINT;
		CommutationOverride : UINT;
		CommutationOffset : REAL;
		nParallel : USINT;
		isDelta : BOOL;
		Voltage : INT;
		Frequency : INT;
		Current : REAL;
		Speed : INT;
		PowerFactor : REAL;
		Power : INT;
	END_STRUCT;
	SystemSettings_IO_typ : 	STRUCT 
		Type : USINT;
		Enabled : BOOL;
		ChannelMap : ARRAY[0..11]OF UINT;
		BaseType : USINT; (*0 = None, 1 = BB80, 2 = BM11, 3 = BM01, 4 = BM15, 5 = BM05*)
	END_STRUCT;
	SystemSettings_Tuning_typ : 	STRUCT 
		POS_CTRL_KV : REAL;
		SCTRL_KV : REAL;
		SCTRL_TN : REAL;
		SCTRL_T_FILTER : REAL;
		FF_TORQUE_LOAD : REAL;
		FF_TORQUE_POS : REAL;
		FF_TORQUE_NEG : REAL;
		FF_KV_TORQUE : REAL;
		FF_INERTIA : REAL;
		NOTCH_MODE : BOOL;
		ISQ_FILTER_A0 : REAL;
		ISQ_FILTER_A1 : REAL;
	END_STRUCT;
	SystemSettings_Limits_typ : 	STRUCT 
		AXLIM_V : REAL;
		AXLIM_A : REAL;
		AXLIM_POS_SW_END : REAL;
		AXLIM_NEG_SW_END : REAL;
		AXLIM_DS_STOP : REAL;
		AXLIM_T_JOLT : REAL;
	END_STRUCT;
	SystemSettings_AxisConfig_typ : 	STRUCT 
		Disabled : BOOL;
		Accel : REAL;
		Decel : REAL;
		JogSpeedHigh : REAL;
		JogSpeed : REAL;
		JogIncrement : REAL;
		HomeOffset : REAL;
		Motor : SystemSettings_Motor_typ;
		Tuning : SystemSettings_Tuning_typ;
		Limits : SystemSettings_Limits_typ;
	END_STRUCT;
	SystemSettings_typ : 	STRUCT 
		SafetyPWInit : BOOL; (*Indicates that the safety password has been *)
		RecipeNum : UINT;
		MaxRecipes : UINT;
		UnitType : UINT;
		Language : UINT;
		Brightness : USINT;
		Reserved1 : STRING[6];
		Reserved2 : STRING[6];
		ReservedS : STRING[16];
		LineVoltage : UINT;
		PhaseMonIgnore : BOOL;
		TemperatureLimit : REAL;
		TemperatureZone : ARRAY[0..11]OF SystemSettings_TempZone_typ;
		AxisConfig : ARRAY[0..MAX_AXIS_INDEX]OF SystemSettings_AxisConfig_typ;
		IO : ARRAY[0..199]OF SystemSettings_IO_typ;
		SMTP : SystemSettings_SMTP_typ;
	END_STRUCT;
END_TYPE

(*Temperature Manager datatypes*)

TYPE
	TempMgr_typ : 	STRUCT 
		State : ARRAY[0..11]OF UINT;
		cmd : TempMgr_cmd_typ;
		data : TempMgr_data_typ;
	END_STRUCT;
	TempMgr_data_typ : 	STRUCT 
		On : ARRAY[0..11]OF BOOL;
		LimitExceeded : BOOL;
		ActualTemp : ARRAY[0..11]OF REAL;
		InRange : ARRAY[0..11]OF BOOL;
		AutoTuning : ARRAY[0..11]OF BOOL;
		NotDetected : ARRAY[0..11]OF BOOL;
	END_STRUCT;
	TempMgr_cmd_typ : 	STRUCT 
		On : ARRAY[0..11]OF BOOL;
		AutoTune : ARRAY[0..11]OF BOOL;
	END_STRUCT;
END_TYPE

(*IO Forcing datatypes*)

TYPE
	IOForce_typ : 	STRUCT 
		cmd : IOForce_cmd_typ;
		data : IOForce_data_typ;
	END_STRUCT;
	IOForce_cmd_typ : 	STRUCT 
		Enable : ARRAY[0..MAX_NUM_CHANNELS]OF BOOL;
		ForceValue : ARRAY[0..MAX_NUM_CHANNELS]OF DINT;
		DisableAllForcing : BOOL;
	END_STRUCT;
	IOForce_data_typ : 	STRUCT 
		PhysicalValue : ARRAY[0..MAX_NUM_CHANNELS]OF DINT;
		ForcingEnabled : BOOL;
	END_STRUCT;
END_TYPE

(*SMTP Manager datatypes*)

TYPE
	SMTP_typ : 	STRUCT 
		Error : ARRAY[0..4]OF BOOL;
	END_STRUCT;
END_TYPE
