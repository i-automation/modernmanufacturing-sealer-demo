PROGRAM _INIT

	// require presence of IO modules when using real target
	IO_ModuleRequired[0] := 0; // X20BC
	IO_ModuleRequired[1] := 0; // X20PS
	IO_ModuleRequired[2] := 0; // X20DI
	IO_ModuleRequired[3] := 0; // X20DI
	IO_ModuleRequired[4] := 0; // X20DO
	IO_ModuleRequired[5] := 0; // X20DO
	IO_ModuleRequired[6] := 0; // X20DO
	IO_ModuleRequired[7] := 0; // X20SLX
	IO_ModuleRequired[8] := 0; // X20SI
	IO_ModuleRequired[9] := 0; // X20SO
	IO_ModuleRequired[10] := 0; // X20BT
	IO_ModuleRequired[11] := 0; // X67DM
	IO_ModuleRequired[12] := 0; 
	
	// usb browse flag
	USBBrowseRequired := 1;
	
	// set ethernet device for AsARCfg functions
	strcpy( ADR(EthernetIF), ADR('IF3') );

	// set visualization name
	strcpy( ADR(VisObjName), ADR('VisWVG') );
	
	// set usb drive file device parameter strng
	strcpy( ADR(FileDeviceParam), ADR('/DEVICE=IF5.ST1'));
	
	//Define hardware configuration
	HwCfg := PP500_EMB;
	
	//Safety Settings
	SLXUsed := FALSE; //If using an SLX type safety controller
	SafetyStation := 'IF1.ST1.IF1.ST7'; // Module address of the safety controller
	
END_PROGRAM


PROGRAM _CYCLIC


(* TODO : Add your code here *)


END_PROGRAM
