PROGRAM _INIT

	// simulation mode doesn't require the IO modules to be present
	IO_ModuleRequired[0] := 0;
	IO_ModuleRequired[1] := 0;
	IO_ModuleRequired[2] := 0; 
	IO_ModuleRequired[3] := 0; 
	IO_ModuleRequired[4] := 0; 
	IO_ModuleRequired[5] := 0; 
	IO_ModuleRequired[6] := 0; 
	IO_ModuleRequired[7] := 0;  
	IO_ModuleRequired[8] := 0; 
	IO_ModuleRequired[9] := 0; 
	
	// fake e-stop being ready
	DI_EStopOK := 1;
	
	// usb browse flag
	USBBrowseRequired := 0;
	
	// set ethernet device for AsARCfg functions
	strcpy( ADR(EthernetIF), ADR('IF1') );
	
	// set visualization name
	strcpy( ADR(VisObjName), ADR('VisWVG') );
	
	// set usb drive file device parameter strng
	strcpy( ADR(FileDeviceParam), ADR('/DEVICE=C:\\Temp'));
	
	//Define hardware configuration
	HwCfg := ARSIM;
	
END_PROGRAM


PROGRAM _CYCLIC


(* TODO : Add your code here *)


END_PROGRAM
