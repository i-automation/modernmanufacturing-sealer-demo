
TYPE
	Steps : 
		(
		STEP_INIT,
		STEP_OFF,
		STEP_POWER_ON,
		STEP_IDLE,
		STEP_JOG,
		STEP_JOG_STOP,
		STEP_JOG_INC,
		STEP_HOME1,
		STEP_HOME2,
		STEP_HOME3,
		STEP_HOME4,
		STEP_WAIT_HOME,
		STEP_MOVE_TO_ZERO,
		STEP_TUNE,
		STEP_TUNE_STOP,
		STEP_AUTO_INIT0,
		STEP_AUTO_INIT1,
		STEP_AUTO_INIT2,
		STEP_AUTO_INIT3,
		STEP_AUTO_INIT4,
		STEP_AUTO_INIT5,
		STEP_AUTO_INIT6,
		STEP_AUTO_INIT7,
		STEP_AUTO_INIT8,
		STEP_AUTO,
		STEP_AUTO_EXIT0,
		STEP_AUTO_EXIT1,
		STEP_AUTO_EXIT2,
		STEP_POWER_OFF,
		STEP_ERROR
		);
END_TYPE

