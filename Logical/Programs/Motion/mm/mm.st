(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: mm
 * File: mm.st
 * Author: ?
 * Created: ?
 * Version: 1.00.0
 * Modified Date:
 * Modified BY:
 ********************************************************************
 * Implementation OF PROGRAM mm

 * Description:

	Motion manager manages motion.

 * Options:

 * Version History:

 ********************************************************************)
PROGRAM _CYCLIC

	IF (DataMgrInit) THEN
	
		// assign variabnles for use with MC_BR_SetHardwareInputs
		//Axis[AX...].cmd.PosHWSwitch := DI_...OTpos;
		//Axis[AX...].cmd.NegHWSwitch := DI_...OTneg;
		
		// these are variables that look at the status of all the enable axes collectively
		AxesOff := 1;
		AxesIdle := 1;
		AxesJog := 0;
		AxesJogInc := 0;
		AxesAutoTune := 0;
		AxesAutomatActive := 0;
		AxesCammingActive := 0;
		AxesError := 0;
		
		FOR AxisIndex := 0 TO MAX_AXIS_INDEX DO
			// edge functions (these have TO be called every PLC scan in order TO be active - if we did this in the state machien then it wouldn't be called every scan necessarily);
			// note we are implementing our own 'EDGEPOS' type function here since EDGEPOS doesn't work on arrayed variables
			EDGEPOSHome[AxisIndex] 		:= (MM.cmd.Home[AxisIndex] AND NOT EDGEPOSHome_last[AxisIndex]);
			EDGEPOSJogNegInc[AxisIndex] :=  (MM.cmd.JogNegInc[AxisIndex] AND NOT EDGEPOSJogNegInc_last[AxisIndex]);
			EDGEPOSJogPosInc[AxisIndex] :=  (MM.cmd.JogPosInc[AxisIndex] AND NOT EDGEPOSJogPosInc_last[AxisIndex]);
			
			EDGEPOSHome_last[AxisIndex] 		:= MM.cmd.Home[AxisIndex];
			EDGEPOSJogNegInc_last[AxisIndex] 	:= MM.cmd.JogNegInc[AxisIndex];
			EDGEPOSJogPosInc_last[AxisIndex] 	:= MM.cmd.JogPosInc[AxisIndex];
			
			AxesOff := AxesOff AND (NOT Axis[AxisIndex].data.On OR SystemSettings.AxisConfig[AxisIndex].Disabled);
			AxesIdle := AxesIdle AND (Axis[AxisIndex].data.Idle OR SystemSettings.AxisConfig[AxisIndex].Disabled);
			
			AxesJog := AxesJog + MM.cmd.JogNegSlow[AxisIndex] + MM.cmd.JogPosSlow[AxisIndex] + MM.cmd.JogNegFast[AxisIndex] + MM.cmd.JogPosFast[AxisIndex];
			AxesJogInc := AxesJogInc + EDGEPOSJogNegInc[AxisIndex] + EDGEPOSJogPosInc[AxisIndex];
			AxesAutoTune := AxesAutoTune + MM.cmd.AutoTune[AxisIndex];
			AxesAutomatActive := AxesAutomatActive + (Axis[AxisIndex].data.AutomatActive AND NOT SystemSettings.AxisConfig[AxisIndex].Disabled);
			AxesCammingActive := AxesCammingActive + (Axis[AxisIndex].data.CammingActive AND NOT SystemSettings.AxisConfig[AxisIndex].Disabled);
			AxesError := AxesError OR (Axis[AxisIndex].data.Error AND NOT SystemSettings.AxisConfig[AxisIndex].Disabled);
			
		END_FOR

		EDGEPOSStartPB := EDGEPOS(HMI.StartPB);
		EDGEPOSStopPB := EDGEPOS(HMI.StopPB);
				
		// mode progression;
		CASE Mode OF
			// all modes are implicitly assumed TO transition TO error mode when necessary;
	
			MODE_OFF: // off mode can only proceed into on/"manual" mode;
				IF (AlarmMgr.EStopOK AND NOT AlarmMgr.ErrFatal) THEN
					SetMode := MODE_MANUAL;
				END_IF;
		
	
			MODE_MANUAL: // manual can only proceed into off OR auto;
				IF (AlarmMgr.ErrFatal OR NOT AlarmMgr.EStopOK) THEN
					SetMode := MODE_OFF;
				
				ELSIF (EDGEPOSStartPB AND NOT AlarmMgr.ErrCritical) THEN   //AND MM.data.HomedToZero
					SetMode := MODE_AUTO;
				END_IF;

		
			MODE_AUTO: // auto can only proceed into off OR manual;
				IF (AlarmMgr.ErrFatal OR NOT AlarmMgr.EStopOK) THEN
					SetMode := MODE_OFF;
			
				ELSIF (AlarmMgr.ErrCritical OR EDGEPOSStopPB) THEN 
					SetMode := MODE_MANUAL;
				END_IF;
				
	
			MODE_ERROR_STOP:
				SetMode := MODE_OFF;
				
		
		END_CASE
	
		// show/hide/lock/unlock various screens based on mode;
		IF (Mode = MODE_MANUAL) THEN
			HIDE(ADR(HMI.StopSDP));
			IF (NOT AlarmMgr.ErrCritical) THEN
				SHOW(ADR(HMI.StartSDP));
			ELSE
				HIDE(ADR(HMI.StartSDP));
			END_IF;
		ELSIF (Mode = MODE_AUTO) THEN
			HIDE(ADR(HMI.StartSDP));
			SHOW(ADR(HMI.StopSDP));
		ELSE
			HIDE(ADR(HMI.StartSDP));
			HIDE(ADR(HMI.StopSDP));	
		END_IF;
	
		IF (AlarmMgr.ErrCritical OR AlarmMgr.ErrFatal OR NOT AlarmMgr.EStopOK) THEN
			SHOW(ADR(HMI.ResetSDP));
		ELSE
			HIDE(ADR(HMI.ResetSDP));
		END_IF;
	
		// drive error handler;
		IF (AxesError AND (Step < STEP_ERROR) ) THEN
			
			//FOR AxisIndex := 0 TO (AxisCount-1) DO
			//	Axis[AxisIndex].cmd.On :=0;
			//END_FOR
			IF (Step = STEP_TUNE) THEN
				MM.data.AutoTuneStatus := 6;
			END_IF
			Step := STEP_ERROR;
		
		END_IF;
	
		// step trace;
		IF (Step <> StepTrace[0]) THEN
			// state 0 is most recent state;
			FOR i := 99 TO 1 BY (-1) DO
				StepTrace[i] := StepTrace[i-1];
			END_FOR
			StepTrace[0] := Step;
		END_IF;

	
		// main state machine;
		CASE Step OF
	
			STEP_INIT:  // 0;
				Mode := MODE_INIT;
			
				IF (AllAxesInit) THEN
					Step := STEP_OFF;
				END_IF;
		
	
			STEP_OFF:
				Mode := MODE_OFF;
			
				IF (MM.cmd.ErrorAcknowledge) THEN
					MM.cmd.ErrorAcknowledge := 0;
					MM.data.ErrorID := ERR_NONE;
							
				ELSIF (SetMode = MODE_MANUAL) THEN
					
					FOR AxisIndex := 0 TO MAX_AXIS_INDEX DO
						Axis[AxisIndex].cmd.On := NOT SystemSettings.AxisConfig[AxisIndex].Disabled;
					END_FOR
					
					Step := STEP_POWER_ON;
				
				END_IF;
				
				
			STEP_POWER_ON: // 15
			
				IF (AxesIdle) THEN
					Step := STEP_IDLE;
				END_IF	
				
			
			STEP_IDLE:  
				Mode := MODE_MANUAL;
			
				IF (SetMode = MODE_OFF) THEN
					Step := STEP_POWER_OFF;
				
				ELSIF (MM.cmd.ErrorAcknowledge) THEN
					MM.cmd.ErrorAcknowledge := 0;
					MM.data.ErrorID := ERR_NONE;
				
				// slow/High velocity Jogs;
				ELSIF (AxesJog = 1) THEN // check to make sure only 1 axis jog command is set
							
					FOR AxisIndex := 0 TO MAX_AXIS_INDEX DO
						IF (MM.cmd.JogNegSlow[AxisIndex] OR MM.cmd.JogNegFast[AxisIndex] OR MM.cmd.JogPosSlow[AxisIndex] OR MM.cmd.JogPosFast[AxisIndex]) THEN
							manualaxis := AxisIndex; // record axis to jog
						END_IF
					END_FOR

					IF (NOT SystemSettings.AxisConfig[manualaxis].Disabled) THEN
						IF (MM.cmd.JogNegSlow[manualaxis]) THEN
							Axis[manualaxis].cmd.Velocity := -1 * SystemSettings.AxisConfig[manualaxis].JogSpeed;
						ELSIF (MM.cmd.JogNegFast[manualaxis]) THEN
							Axis[manualaxis].cmd.Velocity := -1 * SystemSettings.AxisConfig[manualaxis].JogSpeedHigh;
						ELSIF (MM.cmd.JogPosSlow[manualaxis]) THEN
							Axis[manualaxis].cmd.Velocity := SystemSettings.AxisConfig[manualaxis].JogSpeed;
						ELSIF (MM.cmd.JogPosFast[manualaxis]) THEN
							Axis[manualaxis].cmd.Velocity := SystemSettings.AxisConfig[manualaxis].JogSpeedHigh;
						END_IF
					
						// assign configured accel/decel values then issue move;
						Axis[manualaxis].cmd.Acceleration := SystemSettings.AxisConfig[manualaxis].Accel;
						Axis[manualaxis].cmd.Deceleration := SystemSettings.AxisConfig[manualaxis].Decel;
						Axis[manualaxis].cmd.MoveVelocity := 1;
					
						Step := STEP_JOG;
					END_IF
			
				// incremental Jog	
				ELSIF (AxesJogInc = 1) THEN // check to make sure only 1 axis jog command is set
					
					FOR AxisIndex := 0 TO MAX_AXIS_INDEX DO
						IF (EDGEPOSJogNegInc[AxisIndex] OR EDGEPOSJogPosInc[AxisIndex]) THEN
							manualaxis := AxisIndex;
						END_IF
					END_FOR
				
					IF (NOT SystemSettings.AxisConfig[manualaxis].Disabled) THEN
						// now decide what speed/direction;
						IF (EDGEPOSJogNegInc[manualaxis]) THEN
							Axis[manualaxis].cmd.Position := Axis[manualaxis].data.Position - SystemSettings.AxisConfig[manualaxis].JogIncrement;
						ELSE
							Axis[manualaxis].cmd.Position := Axis[manualaxis].data.Position + SystemSettings.AxisConfig[manualaxis].JogIncrement;
						END_IF	
						
						// assign configured velocity/accel/decel values then issue move;
						Axis[manualaxis].cmd.Velocity 		:= SystemSettings.AxisConfig[manualaxis].JogSpeedHigh;
						Axis[manualaxis].cmd.Direction 		:= mcSHORTEST_WAY;
						Axis[manualaxis].cmd.Acceleration 	:= SystemSettings.AxisConfig[manualaxis].Accel;
						Axis[manualaxis].cmd.Deceleration 	:= SystemSettings.AxisConfig[manualaxis].Decel;
						Axis[manualaxis].cmd.MoveAbsolute 	:= 1;
					
						Step := STEP_JOG_INC;
					END_IF

					// homing;
					
				ELSIF (EDGEPOSHomePB) THEN
					
					IF (NOT MM.data.Homed) THEN
						Step := STEP_HOME1;
						
					ELSE
						// issue a home direct to real and sdc feed axes to zero them both out.
						Axis[AXSEALPRESS1].cmd.Home := 1;
						Axis[AXSEALPRESS1].cmd.HomingMode := mcHOME_DIRECT;

	
						Axis[manualaxis].cmd.Velocity := SystemSettings.AxisConfig[manualaxis].JogSpeedHigh;
						Axis[manualaxis].cmd.Acceleration := SystemSettings.AxisConfig[manualaxis].Accel;
						Axis[manualaxis].cmd.Deceleration := SystemSettings.AxisConfig[manualaxis].Decel;
						Axis[manualaxis].cmd.Direction := mcPOSITIVE_DIR;
						Axis[manualaxis].cmd.Position := 0; 
						Axis[manualaxis].cmd.MoveAbsolute := 1;
						//Run.Adjustments.Offset[manualaxis] := 0;
							
						Step := STEP_MOVE_TO_ZERO;
					END_IF


					
				// autotuning
				ELSIF (AxesAutoTune = 1) THEN
					FOR AxisIndex := 0 TO MAX_AXIS_INDEX DO
						// find out which axis we're autotuning.
						IF (MM.cmd.AutoTune[AxisIndex]) THEN
								tuningaxis := AxisIndex;
							END_IF
						END_FOR
						
						IF (NOT SystemSettings.AxisConfig[tuningaxis].Disabled) THEN
							
							// bypass feed forward tuning on select axes
							CASE tuningaxis OF
								AXBALLSCREW:
									Axis[tuningaxis].cmd.AutoTune := 1;
									Axis[tuningaxis].cmd.Distance := (1.0 * SystemSettings.AxisConfig[tuningaxis].Motor.ScalingEncoderCounts / SystemSettings.AxisConfig[tuningaxis].Motor.ScalingMotorRevs / Axis[tuningaxis].data.UnitFactor);
									Axis[tuningaxis].cmd.AutoTuneMode := 1; // FF
								
								AXFEED:	
									Axis[tuningaxis].cmd.AutoTune := 1;
									Axis[tuningaxis].cmd.AutoTuneMode := 0;  // no FF
								
						END_CASE
							Step := STEP_TUNE;
						END_IF
					
						memset( ADR(MM.cmd.AutoTune), 0, SIZEOF(MM.cmd.AutoTune));
				
						// start button functionality
					ELSIF (SetMode = MODE_AUTO) THEN
						Step := STEP_AUTO_INIT0;
				
				END_IF	
			

			// normal velocity Jog;
			STEP_JOG: // 40;
	
				IF (SetMode = MODE_OFF) THEN
					Step := STEP_POWER_OFF;
	
				ELSIF (NOT (MM.cmd.JogNegSlow[manualaxis] XOR MM.cmd.JogNegFast[manualaxis] XOR MM.cmd.JogPosSlow[manualaxis] XOR MM.cmd.JogPosFast[manualaxis]) )THEN
					Axis[manualaxis].cmd.MoveVelocity := 0;
					Step := STEP_JOG_STOP;
				END_IF;
	
		
	
			STEP_JOG_STOP: // 41;
				IF (SetMode = MODE_OFF) THEN
					Step := STEP_POWER_OFF;
	
				ELSIF (Axis[manualaxis].data.Idle) THEN
					Step := STEP_IDLE;
		
					// only allow the same exact type of Jog move TO be restarted	
				ELSIF ( (MM.cmd.JogNegSlow[manualaxis] AND (Axis[manualaxis].cmd.Velocity = -1 * SystemSettings.AxisConfig[manualaxis].JogSpeed)) OR
					(MM.cmd.JogNegFast[manualaxis] AND (Axis[manualaxis].cmd.Velocity = -1 * SystemSettings.AxisConfig[manualaxis].JogSpeedHigh)) OR
					(MM.cmd.JogPosSlow[manualaxis] AND (Axis[manualaxis].cmd.Velocity = SystemSettings.AxisConfig[manualaxis].JogSpeed)) OR
					(MM.cmd.JogPosFast[manualaxis] AND (Axis[manualaxis].cmd.Velocity = SystemSettings.AxisConfig[manualaxis].JogSpeedHigh)) ) THEN
					
					Axis[manualaxis].cmd.MoveVelocity := 1;
					Step := STEP_JOG;
				END_IF;
	
		
		
			// incremental Jog;
			STEP_JOG_INC: // 45;
				
				IF (EDGEPOSJogPosInc[manualaxis]) THEN
					Axis[manualaxis].cmd.Position := Axis[manualaxis].cmd.Position + SystemSettings.AxisConfig[manualaxis].JogIncrement;
				ELSIF (EDGEPOSJogNegInc[manualaxis]) THEN
					Axis[manualaxis].cmd.Position := Axis[manualaxis].cmd.Position - SystemSettings.AxisConfig[manualaxis].JogIncrement;
				END_IF
				
				IF (SetMode = MODE_OFF) THEN
					Step := STEP_POWER_OFF;;
	
				ELSIF (Axis[manualaxis].data.Idle) THEN
					Step := STEP_IDLE;;
				END_IF;
						
			// homing to the limit switches.
			STEP_HOME1:
			
				// updating homing parameters for all axes.
				UpdateHomingParameters;
				
		
				Axis[manualaxis].cmd.Home := 1;

				Axis[manualaxis].cmd.HomingMode := mcHOME_AXIS_REF;
			
				Step := STEP_HOME2;		
			
			
			STEP_HOME2:
				
				IF MM.data.Homed THEN
					Step := STEP_IDLE;
				END_IF
				
			// autotuning
			STEP_TUNE: // 60

				// Note: an error will move the state to the drive error state
	
				// pass status through for HMI display
				IF (Blink) THEN
					MM.data.AutoTuneStatus := 8;
				ELSE
					MM.data.AutoTuneStatus := Axis[tuningaxis].data.AutoTuneStatus;
				END_IF
		
				// this should mean success	
				IF (NOT Axis[tuningaxis].cmd.AutoTune) THEN
					MM.data.AutoTuneStatus := Axis[tuningaxis].data.AutoTuneStatus;
					Step := STEP_IDLE;	
				END_IF
			
		
			STEP_TUNE_STOP: // 61
				IF (Axis[tuningaxis].data.Idle) THEN
					MM.data.AutoTuneStatus := 9;
					Step := STEP_IDLE;
				END_IF


			STEP_AUTO_INIT0:
				Step := STEP_AUTO;

			
			STEP_AUTO: // 80
			
				Mode := MODE_AUTO;

				Axis[AxisIndex].cmd.MoveAbsolute
				
				IF (SetMode = MODE_OFF) THEN
					Step := STEP_POWER_OFF;
					
				ELSIF (SetMode = MODE_MANUAL) THEN
					Step := STEP_AUTO_EXIT0;
					
				END_IF

			//STEP_AUTO_DWELL:
				
				

				
			STEP_AUTO_EXIT0: // 110
			
				IF (Axis[AXMASTER].data.Idle) THEN
					FOR AxisIndex := 0 TO MAX_AXIS_INDEX DO
						Axis[AxisIndex].cmd.AutomatEnd := NOT SystemSettings.AxisConfig[AxisIndex].Disabled;
						Axis[AxisIndex].cmd.MoveCam := 0;
					END_FOR
				
					Step := STEP_AUTO_EXIT1;

				END_IF
			
				
			STEP_POWER_OFF: // 450
				
				FOR AxisIndex := 0 TO MAX_AXIS_INDEX DO
					Axis[AxisIndex].cmd.On := 0;
				END_FOR
				
				IF (AxesOff) THEN
					Step := STEP_OFF;
				END_IF			
		
			
			STEP_ERROR: // 500;
				MM.data.Error := 1;
				Mode := MODE_ERROR_STOP;
				
				// clear fault;
				IF ( MM.cmd.ErrorAcknowledge ) THEN
					FOR AxisIndex := 0 TO MAX_AXIS_INDEX DO
						Axis[AxisIndex].cmd.ErrorAcknowledge := 1;
					END_FOR
					
				END_IF;
	
				IF (NOT AxesError) THEN
					
					FOR AxisIndex := 0 TO MAX_AXIS_INDEX DO
						Axis[AxisIndex].cmd.ErrorAcknowledge := 0;
					END_FOR
					
					MM.cmd.ErrorAcknowledge := 0;
					MM.data.ErrorID := ERR_NONE;
					MM.data.Error := 0;
					MM.data.AutoTuneStatus := 0;
					Step := STEP_INIT;
				END_IF
		
		END_CASE

						
	END_IF

END_PROGRAM


PROGRAM _INIT

	GetCycleTime.enable := 1;
	GetCycleTime();
	cycletime := (GetCycleTime.cycle_time / 1e6);


END_PROGRAM