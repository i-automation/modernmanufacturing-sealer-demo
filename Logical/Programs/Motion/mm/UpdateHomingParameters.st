(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Program: mm
 * File: UpdateHomingParameters.st
 * Author: amusser
 * Created: February 20, 2012
 ********************************************************************
 * Implementation of program mm
 ********************************************************************) 

 ACTION UpdateHomingParameters: 
 
	Master.move.homing.parameter.mode := ncEND_SWITCH;
    Master.move.homing.parameter.edge_sw := ncNEGATIVE;
    Master.move.homing.parameter.trigg_dir := ncPOSITIVE;
	Master.move.homing.parameter.ref_pulse := ncON;
    Master.move.homing.parameter.s := REAL_TO_DINT(SystemSettings.AxisConfig[AXSEALPRESS1].HomeOffset*Axis[AXSEALPRESS1].data.UnitFactor);
    Master.move.homing.parameter.a := MAX(SystemSettings.AxisConfig[AXSEALPRESS1].Accel, SystemSettings.AxisConfig[AXSEALPRESS1].Decel) * Axis[AXSEALPRESS1].data.UnitFactor;
    Master.move.homing.parameter.v_switch := SystemSettings.AxisConfig[AXSEALPRESS1].JogSpeedHigh * Axis[AXSEALPRESS1].data.UnitFactor;
    Master.move.homing.parameter.v_trigger := Master.move.homing.parameter.v_switch / 2;

//	Ballscrew.move.homing.parameter.mode := ncEND_SWITCH;
//    Ballscrew.move.homing.parameter.edge_sw := ncNEGATIVE;
//    Ballscrew.move.homing.parameter.trigg_dir := ncPOSITIVE;
//	Ballscrew.move.homing.parameter.ref_pulse := ncON;
//    Ballscrew.move.homing.parameter.s := REAL_TO_DINT(SystemSettings.AxisConfig[AXBALLSCREW].HomeOffset*Axis[AXBALLSCREW].data.UnitFactor);
//    Ballscrew.move.homing.parameter.a := MAX(SystemSettings.AxisConfig[AXBALLSCREW].Accel, SystemSettings.AxisConfig[AXBALLSCREW].Decel) * Axis[AXBALLSCREW].data.UnitFactor;
//    Ballscrew.move.homing.parameter.v_switch := SystemSettings.AxisConfig[AXBALLSCREW].JogSpeedHigh * Axis[AXBALLSCREW].data.UnitFactor;
//    Ballscrew.move.homing.parameter.v_trigger := Ballscrew.move.homing.parameter.v_switch / 2;
//	
//	Feed.move.homing.parameter.mode := ncSWITCH_GATE; 
//    Feed.move.homing.parameter.edge_sw := ncPOSITIVE;
//    Feed.move.homing.parameter.trigg_dir := ncPOSITIVE;
//    Feed.move.homing.parameter.start_dir := ncPOSITIVE;
//	Feed.move.homing.parameter.ref_pulse := ncON;
//    Feed.move.homing.parameter.s := REAL_TO_DINT(SystemSettings.AxisConfig[AXFEED].HomeOffset*Axis[AXFEED].data.UnitFactor);
//    Feed.move.homing.parameter.a := MAX(SystemSettings.AxisConfig[AXFEED].Accel, SystemSettings.AxisConfig[AXFEED].Decel) * Axis[AXFEED].data.UnitFactor;
//    Feed.move.homing.parameter.v_switch := SystemSettings.AxisConfig[AXFEED].JogSpeedHigh * Axis[AXFEED].data.UnitFactor;
//    Feed.move.homing.parameter.v_trigger := Feed.move.homing.parameter.v_switch / 2;
//
//	ACMotor.move.homing.parameter.mode := ncDIRECT;
//	ACMotor.move.homing.parameter.s := 0;

	
END_ACTION