
TYPE
	MC_FB_typ : 	STRUCT 
		MC_Power : MC_Power;
		MC_Home : MC_Home;
		TON_HomingTimeout : TON;
		TON_AxisErrorAcknowledgeDelay : TON;
		MC_MoveAbsolute : MC_MoveAbsolute;
		MC_MoveAdditive : MC_MoveAdditive;
		MC_MoveVelocity : MC_MoveVelocity;
		MC_BR_InitAutData : MC_BR_InitAutData;
		MC_BR_AutCommand : MC_BR_AutCommand;
		MC_BR_CamDwell : MC_BR_CamDwell;
		MC_BR_DownloadCamProfileData : MC_BR_DownloadCamProfileData;
		MC_BR_DownloadCamProfileObj : MC_BR_DownloadCamProfileObj;
		MC_CamIn : MC_CamIn;
		MC_GearIn : MC_GearIn;
		MC_BR_Offset : MC_BR_Offset;
		MC_BR_Phasing : MC_BR_Phasing;
		MC_Stop : MC_Stop;
		MC_Halt : MC_Halt;
		MC_BR_ReadDriveStatus : MC_BR_ReadDriveStatus;
		MC_ReadStatus : MC_ReadStatus;
		MC_ReadAxisError : MC_ReadAxisError;
		MC_Reset : MC_Reset;
		MC_ReadActualPosition : MC_ReadActualPosition;
		MC_ReadActualVelocity : MC_ReadActualVelocity;
		MC_BR_CyclicReadCurrent : MC_BR_CyclicRead;
		MC_BR_CyclicReadLagError : MC_BR_CyclicRead;
		MC_BR_CyclicReadMotorTemp : MC_BR_CyclicRead;
		MC_BR_ReadParID : MC_BR_ReadParID;
		MC_BR_WriteParID : MC_BR_WriteParID;
		MC_BR_InitParTabObj : MC_BR_InitParTabObj;
		MC_BR_InitParList : MC_BR_InitParList;
		MC_BR_InitAxisPar : MC_BR_InitAxisPar;
		MC_BR_InitAxisSubjectPar : MC_BR_InitAxisSubjectPar;
		MC_BR_SetupController : MC_BR_SetupController;
		MC_BR_InitModPos : MC_BR_InitModPos;
		MC_BR_Simulation : MC_BR_Simulation;
		R_TRIG_NetworkInit : R_TRIG;
		MC_BR_GetCamSlavePosition : MC_BR_GetCamSlavePosition;
	END_STRUCT;
	MC_typ : 	STRUCT 
		Step : Steps;
		EStep : Steps;
		StepTrace : ARRAY[0..99]OF Steps;
		AXIS_adr : UDINT;
		FB : MC_FB_typ;
		config : MC_Config_typ;
		data : MC_Data_typ;
	END_STRUCT;
	MC_Config_typ : 	STRUCT 
		DataObjectName : STRING[12];
		UnitBasis : UnitBasis_enum;
		UnitFactor : UDINT;
		AxisPeriod : UDINT;
	END_STRUCT;
	Steps : 
		(
		STEP_INIT,
		STEP_INIT_MOTOR,
		STEP_INIT_MOTOR_READ_MAX_TEMP,
		STEP_INIT_MOTOR_READ_RATED_CURR,
		STEP_INIT_MOTOR_READ_PEAK_CURR,
		STEP_INIT_MOTOR_COMMUT_OFFSET,
		STEP_INIT_MOTOR_TEMP_MODEL1,
		STEP_INIT_MOTOR_TEMP_MODEL2,
		STEP_INIT_MOTOR_LINE_CHK_IGNORE,
		STEP_INIT_UDC_NOMINAL,
		STEP_INIT_PHASE_MON,
		STEP_INIT_PAR_TABLE_DOWNLOAD,
		STEP_INIT_GLOBAL,
		STEP_INIT_GLOBAL_WAIT,
		STEP_INIT_MODPOS,
		STEP_INIT_CYCLIC,
		STEP_INIT_SIMULATION_MODE,
		STEP_INIT_SWENDLIMIT,
		STEP_HOME_DIRECT,
		STEP_OFF,
		STEP_POWERON_LIMITS,
		STEP_POWERON_TUNING,
		STEP_POWERON_UDCNOMINAL,
		STEP_POWERON_PHASE_MON,
		STEP_POWERON,
		STEP_IDLE,
		STEP_HOME_DEFAULT,
		STEP_HOME_STOP1,
		STEP_HOME_STOP2,
		STEP_SWENDLIMITS_ENABLE,
		STEP_SWENDLIMITS_DISABLE,
		STEP_UPDATE_ENCODER_SCALING1,
		STEP_UPDATE_ENCODER_SCALING2,
		STEP_UPDATE_ENCODER_SCALING3,
		STEP_UPDATE_ENCODER_SCALING4,
		STEP_MOVE_VELOCITY,
		STEP_MOVE_VELOCITY2,
		STEP_MOVE_ADDITIVE,
		STEP_MOVE_ADDITIVE2,
		STEP_MOVE_ABSOLUTE,
		STEP_MOVE_ABSOLUTE2,
		STEP_MOVE_HALT,
		STEP_MOVE_GEAR_IN,
		STEP_MOVE_GEARED,
		STEP_MOVE_CAM_IN,
		STEP_MOVE_CAM,
		STEP_MOVE_CAM_DWELL_INIT,
		STEP_MOVE_CAM_DWELL,
		STEP_AUTOMAT_INIT,
		STEP_AUTOMAT_ENABLE,
		STEP_AUTOMAT_ACTIVE,
		STEP_AUTOMAT_STARTING,
		STEP_AUTOMAT_RUNNING,
		STEP_AUTOMAT_STOP,
		STEP_AUTOTUNE_INIT,
		STEP_AUTOTUNE_SCTRL_INIT,
		STEP_AUTOTUNE_SCTRL,
		STEP_AUTOTUNE_PCTRL_INIT,
		STEP_AUTOTUNE_PCTRL,
		STEP_AUTOTUNE_FF_INIT,
		STEP_AUTOTUNE_FF,
		STEP_AUTOTUNE_STOP,
		STEP_AUTOTUNE_STOP2,
		STEP_STOP,
		STEP_POWER_OFF,
		STEP_ERROR,
		STEP_ERROR_ACKNOWLEDGE,
		STEP_ERROR_RESET
		);
END_TYPE
