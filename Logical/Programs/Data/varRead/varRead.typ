(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * Program: varRead
 * File: varRead.typ
 * Author: pholleger
 * Created: June 12, 2015
 ********************************************************************
 * Local data types of program varRead
 ********************************************************************)

TYPE
	VarReadState_enum : 
		(
		VAR_READ_WAIT,
		VAR_READ_GET_ADR,
		VAR_READ_GET_INFO,
		VAR_READ_OUTPUT,
		VAR_READ_CLEAR
		);
	VarReadOutputState_enum : 
		(
		VAR_READ_ONCE,
		VAR_READ_CONTINUOUS
		);
	VarRead_typ : 	STRUCT 
		cmd : VarReadCommand_typ;
		par : VarReadParameter_typ;
		status : VarReadStatus_typ;
	END_STRUCT;
	VarReadCommand_typ : 	STRUCT 
	END_STRUCT;
	VarReadParameter_typ : 	STRUCT 
		name : STRING[80];
		pv_adr : UDINT;
		dataLen : UDINT;
		pv_datatype : UDINT;
		pv_datalength : UDINT;
		pv_dimension : UDINT;
	END_STRUCT;
	VarReadStatus_typ : 	STRUCT 
		getInfo : UINT;
		getAdr : UINT;
		dynVar : UDINT;
	END_STRUCT;
END_TYPE
