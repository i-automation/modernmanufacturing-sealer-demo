(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * Program: varRead
 * File: varRead.st
 * Author: pholleger
 * Created: June 12, 2015
 * Version: 1.00.0
 * Modified Date: June 12, 2015
 * Modified By: pholleger
 ********************************************************************
 * Implementation of program varRead
 *
 * Description:
 *
 *	This task is intended to take a variable name from the HMI and
	output its value to the HMI
 *
 * Options:
 *
 *	(TO DO type options here)
 *
 * Version History:
 *
 *	(TO DO type version history here)
 *
 ********************************************************************)

PROGRAM _INIT

	HIDE(ADR(stringOutSDP));
	HIDE(ADR(dintOutSDP));
	HIDE(ADR(udintOutSDP));
	HIDE(ADR(realOutSDP));
	HIDE(ADR(wstringOutSDP));
	HIDE(ADR(lrealOutSDP));
	HIDE(ADR(lwordOutSDP));
	HIDE(ADR(ulintOutSDP));
	HIDE(ADR(lintOutSDP));
	UNLOCK(ADR(nameInSDP));
	//
	state_varRead := VAR_READ_WAIT;
	state_varOutput := VAR_READ_ONCE;
	varRead.status.dynVar := DYN_VAR_BOOL;
	//
	memset(ADR(varRead.par.name),0,SIZEOF(varRead.par.name));
	varRead.par.pv_datatype := 22;	//BLANK

END_PROGRAM


PROGRAM _CYCLIC

	CASE state_varRead OF

		VAR_READ_WAIT:
		//receive HMI input of name
		UNLOCK(ADR(nameInSDP));
		UNLOCK(ADR(clearVarSDP));
		UNLOCK(ADR(MonitorSDP));
		UNLOCK(ADR(RequestSDP));
		IF HMI.VarClearPB THEN
			state_varRead := VAR_READ_CLEAR;
			LOCK(ADR(MonitorSDP));
			LOCK(ADR(RequestSDP));
			HMI.VarClearPB := FALSE;
		ELSIF HMI.VarReqPB THEN
			state_varOutput := VAR_READ_ONCE;
			state_varRead := VAR_READ_GET_ADR;
			LOCK(ADR(nameInSDP));
			LOCK(ADR(clearVarSDP));
			HMI.VarReqPB := FALSE;
		ELSIF HMI.VarMonitorPB THEN
			state_varOutput := VAR_READ_CONTINUOUS;
			state_varRead := VAR_READ_GET_ADR;
			LOCK(ADR(nameInSDP));
			LOCK(ADR(clearVarSDP));
			(*DO NOT reset PB until VAR_READ_CONTINUOUS state*)
		END_IF


		VAR_READ_CLEAR:
			HIDE(ADR(stringOutSDP));
			HIDE(ADR(dintOutSDP));
			HIDE(ADR(udintOutSDP));
			HIDE(ADR(realOutSDP));
			HIDE(ADR(wstringOutSDP));
			HIDE(ADR(lrealOutSDP));
			HIDE(ADR(lwordOutSDP));
			HIDE(ADR(ulintOutSDP));
			HIDE(ADR(lintOutSDP));
			UNLOCK(ADR(nameInSDP));
			//
			state_varOutput := VAR_READ_ONCE;
			varRead.status.dynVar := DYN_VAR_BOOL;
			//
			memset(ADR(varRead.par.name),0,SIZEOF(varRead.par.name));
			memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
			memset(ADR(wstringOutVar),0,SIZEOF(wstringOutVar));
			//
			varRead.par.pv_adr := 0;
			varRead.par.dataLen := 0;
			varRead.par.pv_datatype := 22;	//BLANK
			varRead.par.pv_datalength := 0;
			varRead.par.pv_dimension := 0;
			dintOutVar := 0;
			udintOutVar := 0;
			realOutVar := 0;
			lrealOutVar := 0;
			
			byteOutVar := 0;
			wordOutVar := 0;
			dwordOutVar := 0;
			dateOutVar := D#1970-01-01;				//minimum
			dateTimeOutVar := DT#1970-01-01-00:00;	//minimum
			timeOutVar := T#0d0h0m0s0ms;			//median
			todOutVar := TOD#00:00:00.000;			//minimum
			//
			state_varRead := VAR_READ_WAIT;


		VAR_READ_GET_ADR:
		//use name to retrieve address
			varRead.status.getAdr := PV_xgetadr(ADR(varRead.par.name),ADR(varRead.par.pv_adr),ADR(varRead.par.dataLen));
			IF varRead.status.getAdr = 0 THEN
				state_varRead := VAR_READ_GET_INFO;
			ELSIF varRead.status.getAdr = 14710 THEN
				SHOW(ADR(stringOutSDP));
				state_varRead := VAR_READ_WAIT;
				memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
				brsstrcpy(ADR(stringOutVar),ADR('Variable does not exist on the target'));
			ELSE
				state_varRead := VAR_READ_WAIT;
			END_IF


		VAR_READ_GET_INFO:
		//use address to generate info and find the datatype
			varRead.status.getInfo := PV_ninfo(ADR(varRead.par.name),ADR(varRead.par.pv_datatype),ADR(varRead.par.pv_datalength),ADR(varRead.par.pv_dimension));
			IF varRead.status.getInfo = 0 THEN
				varRead.status.dynVar := varRead.par.pv_datatype; // Assign for state machine
				state_varRead := VAR_READ_OUTPUT;
			ELSE
				state_varRead := VAR_READ_WAIT;
			END_IF


		VAR_READ_OUTPUT:
		
			HIDE(ADR(stringOutSDP));
			HIDE(ADR(dintOutSDP));
			HIDE(ADR(udintOutSDP));
			HIDE(ADR(realOutSDP));
			HIDE(ADR(wstringOutSDP));
			HIDE(ADR(lrealOutSDP));
			HIDE(ADR(lwordOutSDP));
			HIDE(ADR(ulintOutSDP));
			HIDE(ADR(lintOutSDP));

			//Process based on variable type
			CASE varRead.status.dynVar OF
				DYN_VAR_ENUM_STRUCT_DERIVED:
					SHOW(ADR(stringOutSDP));
					//Could we use PV_item() here?
					//set the output
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					brsstrcpy(ADR(stringOutVar),ADR('Cannot display the value of a complex variable'));


				DYN_VAR_BOOL:
					SHOW(ADR(stringOutSDP));
					//Instead of outputting a 0 or 1, this outputs a value of TRUE or FALSE
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempBool),varRead.par.pv_adr,SIZEOF(tempBool));
					//set the output
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					IF tempBool THEN
						brsstrcpy(ADR(stringOutVar),ADR('TRUE'));
					ELSIF NOT tempBool THEN
						brsstrcpy(ADR(stringOutVar),ADR('FALSE'));
					END_IF
					

				DYN_VAR_SINT:
					SHOW(ADR(dintOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempSint),varRead.par.pv_adr,SIZEOF(tempSint));
					//set the output
					dintOutVar := SINT_TO_DINT(tempSint);


				DYN_VAR_INT:
					SHOW(ADR(dintOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempInt),varRead.par.pv_adr,SIZEOF(tempInt));
					//set the output
					dintOutVar := INT_TO_DINT(tempInt);


				DYN_VAR_DINT:
					SHOW(ADR(dintOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempDint),varRead.par.pv_adr,SIZEOF(tempDint));
					//set the output
					dintOutVar := tempDint;


				DYN_VAR_USINT:
					SHOW(ADR(udintOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempUsint),varRead.par.pv_adr,SIZEOF(tempUsint));
					//set the output
					udintOutVar := USINT_TO_UDINT(tempUsint);


				DYN_VAR_UINT:
					SHOW(ADR(udintOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempUint),varRead.par.pv_adr,SIZEOF(tempUint));
					//set the output
					udintOutVar := UINT_TO_UDINT(tempUint);


				DYN_VAR_UDINT:
					SHOW(ADR(udintOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempUdint),varRead.par.pv_adr,SIZEOF(tempUdint));
					//set the output
					udintOutVar := tempUdint;


				DYN_VAR_REAL:
					SHOW(ADR(realOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempReal),varRead.par.pv_adr,SIZEOF(tempReal));
					//set the output
					realOutVar := tempReal;


				DYN_VAR_STRING:
					SHOW(ADR(stringOutSDP));
					memset(ADR(tempString),0,SIZEOF(tempString));
					//copy memory at address into a temporary variable of the proper size
						//In this case, always use the SMALLER size. Never want to copy more memory than we need.
					IF varRead.par.pv_datalength < SIZEOF(tempString) THEN
						brsmemcpy(ADR(tempString), varRead.par.pv_adr, varRead.par.pv_datalength);
					ELSIF varRead.par.pv_datalength >= SIZEOF(tempString) THEN
						brsmemcpy(ADR(tempString), varRead.par.pv_adr, SIZEOF(tempString));
					END_IF
					//set the output
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					brsstrcpy(ADR(stringOutVar),ADR(tempString));

				DYN_VAR_DATE_TIME:
					SHOW(ADR(stringOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempDateTime),varRead.par.pv_adr,SIZEOF(tempDateTime));
					//set the output
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					stringOutVar := DT_TO_STRING(tempDateTime);


				DYN_VAR_TIME:
					SHOW(ADR(stringOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempTime),varRead.par.pv_adr,SIZEOF(tempTime));
					//set the output
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					stringOutVar := TIME_TO_STRING(tempTime);


				DYN_VAR_DATE:
					SHOW(ADR(stringOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempDate),varRead.par.pv_adr,SIZEOF(tempDate));
					//set the output
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					stringOutVar := DATE_TO_STRING(tempDate);


				DYN_VAR_LREAL:
					SHOW(ADR(lrealOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempLreal),varRead.par.pv_adr,SIZEOF(tempLreal));
					//set the output
					lrealOutVar := tempLreal;


				DYN_VAR_STRUCT_ARRAY:
					SHOW(ADR(stringOutSDP));
					//set the output
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					brsstrcpy(ADR(stringOutVar),ADR('Cannot display the value of a complex variable'));


				DYN_VAR_TIME_DAY:
					SHOW(ADR(stringOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempTOD),varRead.par.pv_adr,SIZEOF(tempTOD));
					//set the output
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					stringOutVar := TOD_TO_STRING(tempTOD);


				DYN_VAR_BYTE:
					SHOW(ADR(stringOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempByte),varRead.par.pv_adr,SIZEOF(tempByte));
					//set the output
					memset(ADR(lastByteString),0,SIZEOF(lastByteString));
					FOR i := 0 TO 7 DO
						IF i = 0 THEN
							lastByte := tempByte;
						END_IF
						memset(ADR(fullByteString),0,SIZEOF(fullByteString));
						memset(ADR(tempByteString),0,SIZEOF(tempByteString));
						//
						iInc := i+1;
						imod := iInc MOD 4;
						IF  (imod = 0) THEN
							brsstrcat(ADR(fullByteString),ADR('_'));
						END_IF
						byteArray[i] := (BYTE_TO_INT(lastByte) MOD 2);
						int_lastByte := BYTE_TO_INT(lastByte)/2;
						lastByte := INT_TO_BYTE(int_lastByte);
						lastByte;
						//
						tempByteString := INT_TO_STRING(byteArray[i]);
						brsstrcat(ADR(fullByteString),ADR(tempByteString));
						brsstrcat(ADR(fullByteString),ADR(lastByteString));
						memset(ADR(lastByteString),0,SIZEOF(lastByteString));
						brsstrcpy(ADR(lastByteString),ADR(fullByteString));
					END_FOR
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					brsstrcpy(ADR(stringOutVar),ADR(fullByteString));


				DYN_VAR_WORD:
					SHOW(ADR(stringOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempWord),varRead.par.pv_adr,SIZEOF(tempWord));
					//set the output
					memset(ADR(lastByteString),0,SIZEOF(lastByteString));
					FOR i := 0 TO 15 DO
						IF i = 0 THEN
							lastWord := tempWord;
						END_IF
						memset(ADR(fullByteString),0,SIZEOF(fullByteString));
						memset(ADR(tempByteString),0,SIZEOF(tempByteString));
						//
						iInc := i+1;
						imod := iInc MOD 4;
						IF  (imod = 0) THEN
							brsstrcat(ADR(fullByteString),ADR('_'));
						END_IF
						byteArray[i] := (WORD_TO_INT(lastWord) MOD 2);
						int_lastByte := WORD_TO_INT(lastWord)/2;
						lastWord := INT_TO_WORD(int_lastByte);
						lastWord;
						//
						tempByteString := INT_TO_STRING(byteArray[i]);
						brsstrcat(ADR(fullByteString),ADR(tempByteString));
						brsstrcat(ADR(fullByteString),ADR(lastByteString));
						memset(ADR(lastByteString),0,SIZEOF(lastByteString));
						brsstrcpy(ADR(lastByteString),ADR(fullByteString));
					END_FOR
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					brsstrcpy(ADR(stringOutVar),ADR(fullByteString));


				DYN_VAR_DWORD:
					SHOW(ADR(stringOutSDP));
					//copy memory at address into a temporary variable of the proper size
					brsmemcpy(ADR(tempDWord),varRead.par.pv_adr,SIZEOF(tempDWord));
					//set the output
					memset(ADR(lastByteString),0,SIZEOF(lastByteString));
					FOR i := 0 TO 31 DO
						IF i = 0 THEN
							lastDWord := tempDWord;
						END_IF
						memset(ADR(fullByteString),0,SIZEOF(fullByteString));
						memset(ADR(tempByteString),0,SIZEOF(tempByteString));
						//
						iInc := i+1;
						imod := iInc MOD 4;
						IF  (imod = 0) THEN
							brsstrcat(ADR(fullByteString),ADR('_'));
						END_IF
						byteArray[i] := (DWORD_TO_INT(lastDWord) MOD 2);
						int_lastByte := DWORD_TO_INT(lastDWord)/2;
						lastDWord := INT_TO_DWORD(int_lastByte);
						lastDWord;
						//
						tempByteString := INT_TO_STRING(byteArray[i]);
						brsstrcat(ADR(fullByteString),ADR(tempByteString));
						brsstrcat(ADR(fullByteString),ADR(lastByteString));
						memset(ADR(lastByteString),0,SIZEOF(lastByteString));
						brsstrcpy(ADR(lastByteString),ADR(fullByteString));
					END_FOR
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					brsstrcpy(ADR(stringOutVar),ADR(fullByteString));

				DYN_VAR_WSTRING:
					SHOW(ADR(wstringOutSDP));
					memset(ADR(tempWString),0,SIZEOF(tempWString));
					//copy memory at address into a temporary variable of the proper size
						//In this case, always use the SMALLER size. Never want to copy more memory than we need.
					IF varRead.par.pv_datalength < SIZEOF(tempWString) THEN
						brsmemcpy(ADR(tempWString), varRead.par.pv_adr, varRead.par.pv_datalength);
					ELSIF varRead.par.pv_datalength >= SIZEOF(tempWString) THEN
						brsmemcpy(ADR(tempWString), varRead.par.pv_adr, SIZEOF(tempWString));
					END_IF
					//set the output
					memset(ADR(wstringOutVar),0,SIZEOF(wstringOutVar));
					brsstrcpy(ADR(wstringOutVar),ADR(tempWString));


				DYN_VAR_LINT,DYN_VAR_LWORD,DYN_VAR_ULINT:
					SHOW(ADR(stringOutSDP));
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					brsstrcpy(ADR(stringOutVar),ADR('Monitor not avaiable for this datatype'));
				
				ELSE
					SHOW(ADR(stringOutSDP));
					memset(ADR(stringOutVar),0,SIZEOF(stringOutVar));
					brsstrcpy(ADR(stringOutVar),ADR('Monitor not avaiable for this datatype'));

			END_CASE
	
			//Decide whether we go back to idle or hang out reading here
			CASE state_varOutput OF
				VAR_READ_ONCE:
				//REQUEST mode will only query once.
					state_varRead := VAR_READ_WAIT;

				VAR_READ_CONTINUOUS:
				//MONITOR mode will continue to monitor the variable AND maybe lock out the name input
					IF EDGENEG(HMI.VarMonitorPB) THEN
						state_varRead := VAR_READ_WAIT;
					END_IF

			END_CASE

	END_CASE
END_PROGRAM
