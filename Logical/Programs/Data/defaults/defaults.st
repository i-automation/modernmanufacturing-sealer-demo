(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * Program: defaults
 * File: defaults.st
 * Author: amusser
 * Created: January 06, 2012
 * Version: 1.00.0
 * Modified Date: 5/26/2016
 * Modified BY: mhanson
 ********************************************************************
 * Implementation OF PROGRAM DynIOmap

 * Description:

	This PROGRAM lists default settings, and it is possible TO DO so
	FOR several different configurations.

 * Options:

 * Version History:
 	1.0 -- first release
	1.1 --  Added safety settings

 ********************************************************************)

PROGRAM _CYCLIC
	IF (DataMgrInit) THEN
		IF (HMI.OEMRestoreDefaultsPB) THEN
			HMI.OEMRestoreDefaultsPB := 0;
					
			 // Dont allow the restore defaults button to clear this out. This will screw up the sync with the safe PLC
			safePWInit := SystemSettings.SafetyPWInit;
			
			memset(ADR(SystemSettings), 0, SIZEOF(SystemSettings));
			
			SystemSettings.SafetyPWInit := safePWInit;
			SystemSettings.ReservedS := '537013029';
					
			SystemSettings.MaxRecipes := 25;
			SystemSettings.Language := 0;
			SystemSettings.UnitType := 0;
			SystemSettings.Brightness := 100;
			SystemSettings.LineVoltage := 115;
			SystemSettings.PhaseMonIgnore := 1;
			
			// SMTP Defaults
			SystemSettings.SMTP.ModelName 					:= ('Test String'); // Enter OEM Model Name
			SystemSettings.SMTP.IPParts[0]					:= 0;
			SystemSettings.SMTP.IPParts[1]					:= 0;
			SystemSettings.SMTP.IPParts[2]					:= 0;
			SystemSettings.SMTP.IPParts[3]					:= 0;
			SystemSettings.SMTP.Username					:= ('');
			SystemSettings.SMTP.Password					:= ('');
			SystemSettings.SMTP.SenderAdr					:= ('');
			SystemSettings.SMTP.SetInterval				:= T#8h;
			SystemSettings.SMTP.MessageData[1].FollowUp	:= T#8h;
			SystemSettings.SMTP.MessageData[1].SendInterval:= T#8h;

			// Seal Press Servo
			// example scaling is for motor -> 10:1 gearbox -> 1000mm / rev
			SystemSettings.AxisConfig[AXSEALPRESS1].Motor.Type 						:= MOTOR_8VLA13;
			SystemSettings.AxisConfig[AXSEALPRESS1].Motor.CommutationOverride		:= 1;
			SystemSettings.AxisConfig[AXSEALPRESS1].Motor.CommutationOffset			:= 0;
			SystemSettings.AxisConfig[AXSEALPRESS1].Motor.ScalingEncoderCounts		:= REAL_TO_UDINT(1.0 * 1000.0 * Axis[AXSEALPRESS1].data.UnitFactor); // the 254 factor here allows us to cancel out the decimal place in the 25.4
			SystemSettings.AxisConfig[AXSEALPRESS1].Motor.ScalingMotorRevs			:= REAL_TO_UDINT(10.0 * 1.0);
			SystemSettings.AxisConfig[AXSEALPRESS1].Motor.EncoderDirection			:= 1;
			SystemSettings.AxisConfig[AXSEALPRESS1].Motor.EncoderCheckBypass		:= 1;
			SystemSettings.AxisConfig[AXSEALPRESS1].Motor.TemperatureModelDisable	:= 1;
			SystemSettings.AxisConfig[AXSEALPRESS1].JogSpeed						:= 0.5; //in/sec
			SystemSettings.AxisConfig[AXSEALPRESS1].JogSpeedHigh					:= 1.0; // in/sec
			SystemSettings.AxisConfig[AXSEALPRESS1].JogIncrement					:= 0.1; // ins
			SystemSettings.AxisConfig[AXSEALPRESS1].Accel							:= 5; //in/sec^2
			SystemSettings.AxisConfig[AXSEALPRESS1].Decel							:= 10; //in/sec^2
			SystemSettings.AxisConfig[AXSEALPRESS1].Limits.AXLIM_T_JOLT				:= 0; 
			SystemSettings.AxisConfig[AXSEALPRESS1].Limits.AXLIM_V 					:= floor((5000.0 / 60.0) * (SystemSettings.AxisConfig[AXSEALPRESS1].Motor.ScalingEncoderCounts / SystemSettings.AxisConfig[AXSEALPRESS1].Motor.ScalingMotorRevs) / Axis[AXSEALPRESS1].data.UnitFactor); //in/sec
			SystemSettings.AxisConfig[AXSEALPRESS1].Limits.AXLIM_A 					:= SystemSettings.AxisConfig[AXSEALPRESS1].Limits.AXLIM_V * 10.0; //in/sec^2
			SystemSettings.AxisConfig[AXSEALPRESS1].Limits.AXLIM_DS_STOP			:= 0.05;  // in
			SystemSettings.AxisConfig[AXSEALPRESS1].Tuning.POS_CTRL_KV				:= 300;
			SystemSettings.AxisConfig[AXSEALPRESS1].Tuning.SCTRL_KV					:= 0.2;
			SystemSettings.AxisConfig[AXSEALPRESS1].Tuning.SCTRL_T_FILTER			:= 0.0008;
			SystemSettings.AxisConfig[AXSEALPRESS1].Limits.AXLIM_POS_SW_END			:= 12; //in
			SystemSettings.AxisConfig[AXSEALPRESS1].Limits.AXLIM_NEG_SW_END 		:= 0; //in
(*
			// example scaling is for motor -> 7:3 gearbox -> 20 mm pitch ball screw
			SystemSettings.AxisConfig[AXBALLSCREW].Motor.Type 					:= MOTOR_8MSA2S;
			SystemSettings.AxisConfig[AXBALLSCREW].Motor.CommutationOverride	:= 1;
			SystemSettings.AxisConfig[AXBALLSCREW].Motor.CommutationOffset		:= 0;
			SystemSettings.AxisConfig[AXBALLSCREW].Motor.ScalingEncoderCounts	:= REAL_TO_UDINT(3.0 * 20.0 / 25.4 * 254.0 * Axis[AXBALLSCREW].data.UnitFactor); // the 254 factor here allows us to cancel out the decimal place in the 25.4
			SystemSettings.AxisConfig[AXBALLSCREW].Motor.ScalingMotorRevs		:= REAL_TO_UDINT(7.0 * 254.0);
			SystemSettings.AxisConfig[AXBALLSCREW].Motor.EncoderDirection		:= 1;
			SystemSettings.AxisConfig[AXBALLSCREW].Motor.EncoderCheckBypass		:= 1;
			SystemSettings.AxisConfig[AXBALLSCREW].Motor.TemperatureModelDisable:= 1;
			SystemSettings.AxisConfig[AXBALLSCREW].JogSpeed						:= 0.5; //in/sec
			SystemSettings.AxisConfig[AXBALLSCREW].JogSpeedHigh					:= 1.0; // in/sec
			SystemSettings.AxisConfig[AXBALLSCREW].JogIncrement					:= 0.1; // ins
			SystemSettings.AxisConfig[AXBALLSCREW].Accel						:= 5; //in/sec^2
			SystemSettings.AxisConfig[AXBALLSCREW].Decel						:= 10; //in/sec^2
			SystemSettings.AxisConfig[AXBALLSCREW].Limits.AXLIM_T_JOLT			:= 0; 
			SystemSettings.AxisConfig[AXBALLSCREW].Limits.AXLIM_V 				:= floor((5000.0 / 60.0) * (SystemSettings.AxisConfig[AXBALLSCREW].Motor.ScalingEncoderCounts / SystemSettings.AxisConfig[AXBALLSCREW].Motor.ScalingMotorRevs) / Axis[AXBALLSCREW].data.UnitFactor); //in/sec
			SystemSettings.AxisConfig[AXBALLSCREW].Limits.AXLIM_A 				:= SystemSettings.AxisConfig[AXBALLSCREW].Limits.AXLIM_V * 10.0; //in/sec^2
			SystemSettings.AxisConfig[AXBALLSCREW].Limits.AXLIM_DS_STOP			:= 0.05;  // in
			SystemSettings.AxisConfig[AXBALLSCREW].Tuning.POS_CTRL_KV			:= 300;
			SystemSettings.AxisConfig[AXBALLSCREW].Tuning.SCTRL_KV				:= 0.2;
			SystemSettings.AxisConfig[AXBALLSCREW].Tuning.SCTRL_T_FILTER		:= 0.0008;
			SystemSettings.AxisConfig[AXBALLSCREW].Limits.AXLIM_POS_SW_END		:= 12; //in
			SystemSettings.AxisConfig[AXBALLSCREW].Limits.AXLIM_NEG_SW_END 		:= 0; //in
		
			// example scaling is for motor -> 3:1 gearbox -> 27 tooth pulley with 0.2 inches per tooth
			SystemSettings.AxisConfig[AXFEED].Motor.Type 						:= MOTOR_8JSA42;
			SystemSettings.AxisConfig[AXFEED].Motor.ScalingEncoderCounts 		:= REAL_TO_UDINT(1.0 * 0.2 * 5.0 * 27 * Axis[AXFEED].data.UnitFactor); // the 5.0 factor here allows us to cancel out the decimal place in the 0.2
			SystemSettings.AxisConfig[AXFEED].Motor.ScalingMotorRevs			:= REAL_TO_UDINT(3.0 * 5.0);
			SystemSettings.AxisConfig[AXFEED].Motor.EncoderDirection			:= 0;
			SystemSettings.AxisConfig[AXFEED].Motor.EncoderCheckBypass			:= 0;
			SystemSettings.AxisConfig[AXFEED].Motor.TemperatureModelDisable		:= 0;
			SystemSettings.AxisConfig[AXFEED].JogSpeed							:= 0.5; //in/sec
			SystemSettings.AxisConfig[AXFEED].JogSpeedHigh						:= 1.0; // in/sec
			SystemSettings.AxisConfig[AXFEED].JogIncrement						:= 0.1; // ins
			SystemSettings.AxisConfig[AXFEED].Accel								:= 5; //in/sec^2
			SystemSettings.AxisConfig[AXFEED].Decel								:= 10; //in/sec^2
			SystemSettings.AxisConfig[AXFEED].Limits.AXLIM_T_JOLT				:= 0; 
			SystemSettings.AxisConfig[AXFEED].Limits.AXLIM_V 					:= floor((5000.0 / 60.0) * (SystemSettings.AxisConfig[AXFEED].Motor.ScalingEncoderCounts / SystemSettings.AxisConfig[AXFEED].Motor.ScalingMotorRevs) / Axis[AXFEED].data.UnitFactor); //in/sec
			SystemSettings.AxisConfig[AXFEED].Limits.AXLIM_A 					:= SystemSettings.AxisConfig[AXFEED].Limits.AXLIM_V * 10.0; //in/sec^2
			SystemSettings.AxisConfig[AXFEED].Limits.AXLIM_DS_STOP				:= 0.05;  // /in
			SystemSettings.AxisConfig[AXFEED].Tuning.POS_CTRL_KV				:= 300;
			SystemSettings.AxisConfig[AXFEED].Tuning.SCTRL_KV					:= 0.5;
			SystemSettings.AxisConfig[AXFEED].Tuning.SCTRL_T_FILTER				:= 0.0008;

	
			// example AC  motor - parameters based on Automation Direct E2001 
			SystemSettings.AxisConfig[AXACMOTOR].Motor.Type 					:= MOTOR_AC;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.Voltage					:= 460;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.Power					:= 750;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.Frequency				:= 60;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.Speed					:= 1760;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.Current					:= 1.6;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.PowerFactor				:= 0.685;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.isDelta					:= 0;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.nParallel				:= 0;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.ScalingEncoderCounts 	:= REAL_TO_UDINT(1.0 * Axis[AXACMOTOR].data.UnitFactor); // the 5.0 factor here allows us to cancel out the decimal place rev the 0.2
			SystemSettings.AxisConfig[AXACMOTOR].Motor.ScalingMotorRevs		 	:= REAL_TO_UDINT(1.0);
			SystemSettings.AxisConfig[AXACMOTOR].Motor.EncoderDirection			:= 0;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.EncoderCheckBypass		:= 0;
			SystemSettings.AxisConfig[AXACMOTOR].Motor.TemperatureModelDisable	:= 0;
			SystemSettings.AxisConfig[AXACMOTOR].JogSpeed						:= 2; //rev/sec
			SystemSettings.AxisConfig[AXACMOTOR].JogSpeedHigh					:= 5; // rev/sec
			SystemSettings.AxisConfig[AXACMOTOR].JogIncrement					:= 0.1; // revs
			SystemSettings.AxisConfig[AXACMOTOR].Accel							:= 0.5; //rev/sec^2
			SystemSettings.AxisConfig[AXACMOTOR].Decel							:= 10; //rev/sec^2
			SystemSettings.AxisConfig[AXACMOTOR].Limits.AXLIM_T_JOLT			:= 0; 
			SystemSettings.AxisConfig[AXACMOTOR].Limits.AXLIM_V 				:= floor((1800.0 / 60.0) * (SystemSettings.AxisConfig[AXACMOTOR].Motor.ScalingEncoderCounts / SystemSettings.AxisConfig[AXACMOTOR].Motor.ScalingMotorRevs) / Axis[AXACMOTOR].data.UnitFactor); //rev/sec
			SystemSettings.AxisConfig[AXACMOTOR].Limits.AXLIM_A 				:= SystemSettings.AxisConfig[AXACMOTOR].Limits.AXLIM_V * 10.0; //rev/sec^2
*)
			//	AlarmMgr.RestartRequired := 1;
			HMI.OEMSysSettingsStatus := 7;   // factory defaults restored
						
		END_IF
	END_IF
	
END_PROGRAM
