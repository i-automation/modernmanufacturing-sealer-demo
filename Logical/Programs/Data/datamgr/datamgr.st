(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: datamgr
 * File: datamgr.st
 * Author: ?
 * Created: ?
 * Version: 1.00.0
 * Modified Date:
 * Modified BY:
 ********************************************************************
 * Implementation OF PROGRAM datamgr

 * Description:

	This task ensures that our recipes have properly loaded. If so,
	DataMgrInit is set to true.

 * Options:

 * Version History:

 ********************************************************************)
PROGRAM _CYCLIC

	// RECIPE CYCLIC
	RecipesCyclic.Internal				:= ADR(RecipeInternal);
	RecipesCyclic.ActiveFileMismatch	:= ADR(RecipeMismatch);   // this variable must be in permanent memory
	RecipesCyclic.NActiveFile			:= ADR(SystemSettings.RecipeNum);
	RecipesCyclic.MaxFiles				:= ADR(SystemSettings.MaxRecipes);
	RecipesCyclic.NewSys				:= ADR(HMI.OEMRestoreDefaultsPB);
	
	RecipesCyclic();		
	
	RecipeNum_vc := SystemSettings.RecipeNum + 1;
	RecipeNRec_vc := RecipeInternal.N + 1;
	RecipeNRecDst_vc := RecipeInternal.NDst + 1;
	
	// limit RecipeN index to not exceed our max files.
	HMI.RecipeN := LIMIT(0, HMI.RecipeN, RecipeInternal.MaxFiles);
	
	DataMgrInit := RecipesCyclic.Init;

	
END_PROGRAM


PROGRAM _INIT

	(* ------- configuration options ----------
	 !!!! MAKE SURE THERE ARE ENOUGH B&R OBJECTS ALLOCATED IN THE SW CONFIGURATION 
	      AND THAT THE RECIPE NAMES ARRAY IS SIZED APPROPRIATELY !!!!
	*)

	// RECIPES
	RecipeConfig.FileAutoSave := 1;
	RecipeConfig.SaveInterval := T#1s;
	RecipeConfig.pRemoteFileDevice := ADR('usbdrive');
	RecipeConfig.pNameSys := ADR('SystemSettings');   // this should be the PVI name of the SystemSettigns structure
	RecipeConfig.pNameFile := ADR('datamgr:RecipeTemp');  // this should be the PVI name of the Recipe_temp structure
	strcpy(ADR(RecipeConfig.SysObjName), ADR('SYS'));
	strcpy(ADR(RecipeConfig.FileObjBaseName), ADR('REC'));
	RecipeConfig.pFile := ADR(Recipe);
	RecipeConfig.pFileView := ADR(RecipeView);
	RecipeConfig.pFileBckGnd := ADR(RecipeBckGnd);
	RecipeConfig.pFileName := ADR(Recipe.Name);
	RecipeConfig.pFileTemp := ADR(RecipeTemp);
	RecipeConfig.szFile := SIZEOF(Recipe);
	RecipeConfig.szFileName := SIZEOF(Recipe.Name);
	RecipeConfig.pSys := ADR(SystemSettings);
	RecipeConfig.pSys_init := ADR(SystemSettings_init);
	RecipeConfig.szSys := SIZEOF(SystemSettings);

	RecipesInit.Internal := ADR(RecipeInternal);
	RecipesInit.Config := ADR(RecipeConfig);

	RecipesInit();



END_PROGRAM