﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.2.5.388?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Implementation code">alarmmgr.st</File>
    <File Description="Local variables" Private="true">alarmmgr.var</File>
    <File Description="Local data types" Private="true">alarmmgr.typ</File>
    <File Description="Axis Parameter Check">axisparam.st</File>
    <File>AxisConfig.tmx</File>
  </Files>
</Program>