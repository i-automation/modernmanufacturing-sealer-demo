﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.2.5.388?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Implementation code">safetymgr.st</File>
    <File Description="Local data types" Private="true">safetymgr.typ</File>
    <File Description="Local variables" Private="true">safetymgr.var</File>
    <File Description="Useful functions" Private="true">safetymgr.fun</File>
    <File Description="converts a UDINT values into a HEX character string">itoahex.st</File>
  </Files>
</Program>