(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * Program: safetyhmi
 * File: safetyhmi.st
 * Author: mhanson
 * Created: May 2015
 * Version: 1.00.0
 * Modified Date: June 11, 2015
 * Modified BY: pholleger
 ********************************************************************
 * Implementation OF PROGRAM safetyhmi

 * Description:

	This program handles HMI functionality related to safety

 * Options:

 * Version History:
 ********************************************************************)

PROGRAM _INIT

	HMI.SafeLoginPassword := '';
	SHOW(ADR(HMI.SafeLoginSDP)); //always show the popup unless logged in

END_PROGRAM


PROGRAM _CYCLIC


	//NO logging in if an incorrect password or if not logged in as OEM
	IF HMI.LoginLevel >= LOGINLEVEL_ADMIN AND (strcmp(ADR(HMI.SafeLoginPassword),ADR(SystemSettings.ReservedS)) = 0) THEN
		HMI.SafeLoginPBLock := FALSE;
	ELSE
		HMI.SafeLoginPBLock := TRUE;
	END_IF
	
	//Redundant, but again, check the safety password
	IF HMI.SafeLoginOKPB AND (strcmp(ADR(HMI.SafeLoginPassword),ADR(SystemSettings.ReservedS)) = 0) THEN
		HMI.SafeLoginOKPB := FALSE;
		HMI.SafeLoggedIn := TRUE;
		HIDE(ADR(HMI.SafeLoginSDP));
	ELSIF HMI.SafeLoginCancelPB THEN // Cancel login
		HMI.SafeLoginCancelPB := FALSE;
		HMI.SafeLoggedIn := FALSE;
		HMI.Page.Change := HMI.Page.Last;
		IF HMI.Page.Last >= PAGE_SERVICE1 AND HMI.Page.Last < PAGE_SERVICE8 THEN
			HMI.Page.Change := HMI.Page.Last;
		ELSE
			HMI.Page.Change := PAGE_SERVICE1;
		END_IF
	ELSIF (EDGEPOS(HMI.LoginLevel < 1) AND (HMI.Page.Current = PAGE_SERVICE8 OR HMI.Page.Current = PAGE_SERVICE9)) THEN //Leave the safety pages if logging out
		HMI.SafeLoggedIn := FALSE;
		HMI.Page.Change := 40;
	ELSE
		HMI.SafeLoginOKPB := FALSE;
	END_IF
	
	//Log out if leaving the safety pages
	IF HMI.Page.Current <> PAGE_SERVICE8 AND HMI.Page.Current <> PAGE_SERVICE9 THEN
		HMI.SafeLoggedIn := FALSE;
		HMI.SafeLoginPassword := '';
		SHOW(ADR(HMI.SafeLoginSDP));
	END_IF


END_PROGRAM
