(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: serialmgr
 * File: serialmgr.st
 * Author: MH
 * Created: June 15, 2015
 ********************************************************************
 * Implementation OF PROGRAM serialmgr
 ********************************************************************)

PROGRAM _INIT


	(*  
		SerialComm.Parameter.Config.Mode: This configures the serial interface -- applied when iASerialCyclic is enabled. 
		The specified parameters will overwrite the configuration OF the hardware module. All unspecified 
		parameters will default TO the setting in the HWcfg FOR the communication channel.
		
		PARAMETER DESCRIPTION
		/BD     -- Baud rate: 300-115200
		/PA     -- Parity: N|E|O|H|L
		/DB     -- Data bits: 7|8
		/SB     -- Stop bits: 1|2
		/PHY    -- Physical properties: RS232|RS422|RS485| TTY|RS422BUS
		/RIT    -- Recieve idle time: 1-65535
		/TIT    -- Transmit idle time: 1-65535
		/RREQ   -- Max. number OF read requests on the device: 0-255
		/WREQ   -- Max. number OF write requests on the device: 0-255
		/CCHAR1 -- Control character 1 (frame end character): Code OF every single ASCII character
		/CCHAR2 -- Control character 2
		/CCHAR3 -- Control character 3
		/CCHAR4 -- Control character 4
		/FCCTS  -- CTS output flow control (hardware handshake in output direction): 0|1 (FALSE/TRUE)
		/FCSW   -- XON/XOFF output flow control (software handshake in output direction): 0|1 (FALSE/TRUE)
		/RTSTX  -- TX frame - framed BY the RTS signal: 0|1 (FALSE/TRUE)
		
		String example: '/PHY=RS232 /BD=9600 /PA=N /DB=8 /SB=1'	
		Note: Parameters must be separated BY a space	
		
		FOR help see DVFrame library help
		
		FOR each serial communication channel that is used, a SerialComm_typ variable, SerialCyclic() FB, 
		AND seperate Action must be created
	*)
	
(***************************************SERIAL COMMUNICATION CONFIGURATION***************************************)	
	
	//Interface configuration
	SerialComm.Parameter.Config.Mode	     := '/PHY=RS232';// /CCHAR1=0x0d'; //CCHAR= 13 (decimal) -- See physical config for the unspecified pars
	SerialComm.Parameter.Config.Interface    := 'IF1';
	
	//Advanced communication configuration
	SerialComm.Parameter.Config.CommConfig.delim[0]	    := 13;
	SerialComm.Parameter.Config.CommConfig.delimc		:= 1;
	SerialComm.Parameter.Config.CommConfig.rx_cnt		:= 8; //Enable all 8 recieve buffers just in case they're needed
	
	//Read timeout
	SerialComm.Parameter.ReadTimeout       := T#10s; //Max. time between commanding a read and recieving data
	SerialComm.FB.ReadTimeout.PT           := SerialComm.Parameter.ReadTimeout;
	
	//Initialize SerialComm.FB.SerialComm function block
	SerialComm.FB.SerialComm.Config := ADR(SerialComm.Parameter.Config);



END_PROGRAM


PROGRAM _CYCLIC

	IF DataMgrInit THEN
		//Serial cyclic communication --  see SerialCyclic action for code
		IF SerialEnabled THEN
			IF AckSerialSerialError THEN
				AckSerialSerialError        := FALSE;
				SerialComm.Command.AckError := TRUE;
			END_IF
			
			//Serial comm action
			SerialCyclic;
		ELSE
			SerialComm.FB.SerialComm.Enable := FALSE;
			SerialComm.Status.Step          := 0;
			SerialComm.FB.SerialComm();
		END_IF	
	END_IF	

END_PROGRAM