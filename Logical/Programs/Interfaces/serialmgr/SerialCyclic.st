(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * Program: serialmgr
 * File: RC10Cyclic.st
 * Author: mhanson
 * Created: June 18, 2015
 ********************************************************************
 * Implementation of program serialmgr
 *******************************************************************
 *						  RC10 COMMUNICATION                       *
 *******************************************************************) 

 ACTION SerialCyclic: 
		
	//Serial communication is constantly reading,timeout when data hasnt been sent for X amt of time
	CASE SerialComm.Status.Step OF
		SERIAL_IDLE: //0 -- Wait for a command
			SerialComm.FB.SerialComm.Enable := TRUE;
				
			IF SerialComm.Command.Read THEN
				SerialComm.Command.Read         := FALSE;
				SerialComm.Status.Reading       := TRUE;
				SerialComm.FB.SerialComm.cmd    := IASERIAL_CMD_READ;
				SerialComm.Status.Step          := SERIAL_READ1;
			ELSIF SerialComm.Command.Write THEN
				SerialComm.Command.Write        := FALSE;
				SerialComm.Status.Writing       := TRUE;
				SerialComm.FB.SerialComm.cmd    := IASERIAL_CMD_WRITE;
				SerialComm.Status.Step          := SERIAL_WRITE1;
			ELSE
				SerialComm.Status.Reading       := FALSE;
				SerialComm.Status.Writing       := FALSE;
				SerialComm.FB.SerialComm.cmd    := IASERIAL_CMD_NONE;
				SerialComm.Status.Step          := SERIAL_IDLE;
			END_IF
			
		SERIAL_READ1://10 -- Read in data
			memset(ADR(SerialComm.Status.ReadData),0,SIZEOF(SerialComm.Status.ReadData)); // Clear out read data	
				
			SerialComm.FB.ReadTimeout.IN := TRUE;	
			SerialComm.Status.Step       := SERIAL_READ2;
		SERIAL_READ2://20 -- Wait for read to finish
			IF SerialComm.FB.SerialComm.Status = IASERIAL_STATUS_DONE THEN //Success
				SerialComm.Status.Step       := SERIAL_ANALYZE;
				SerialComm.Status.NoData     := FALSE;
				strcpy(ADR(SerialComm.Status.ReadData),ADR(SerialComm.FB.SerialComm.ReceiveData));
			ELSIF (SerialComm.FB.SerialComm.Status = IASERIAL_STATUS_NOINPUT) THEN //No data to read
				SerialComm.Status.Step       := SERIAL_ANALYZE;
				SerialComm.Status.NoData     := TRUE;
			ELSIF SerialComm.FB.SerialComm.Status = IASERIAL_STATUS_ERROR THEN //Error
				SerialComm.Status.Step := SERIAL_ERROR;
			END_IF
				
		SERIAL_ANALYZE: //30 -- Analyze the data
		
				IF SerialComm.FB.ReadTimeout.Q THEN //Read timeout
					SerialComm.Status.Step := SERIAL_ERROR;			
				ELSIF SerialComm.Status.NoData OR (strcmp(ADR(SerialComm.Status.ReadData),ADR('')) = 0) THEN //No data 
					SerialComm.Status.Step := SERIAL_READ1; //Reread if there is no data
//					SerialComm.Status.Step := SERIAL_WRITE1; 
//					SerialComm.Status.Step := SERIAL_IDLE; 
				ELSE //Valid data
					
					//*******************Parse and analyze data here***********************
					
					SerialComm.FB.ReadTimeout.IN := FALSE;
					SerialComm.Status.Step       := SERIAL_IDLE;
				END_IF

		SERIAL_WRITE1: //40 -- Write data to channel
			strcpy(ADR(SerialComm.Parameter.WriteData),ADR('Data to write')); //Assign the data to write
			strcat(ADR(SerialComm.Parameter.WriteData),ADR(END_CHAR)); //Add the end character, this can vary. $r -- Return character. $n -- Line feed
		
			SerialComm.FB.SerialComm.SendData  := ADR(SerialComm.Parameter.WriteData);
			SerialComm.Status.Step             := SERIAL_WRITE2;
			
		SERIAL_WRITE2: //50 -- Wait to complete
		 	IF SerialComm.FB.SerialComm.Status = IASERIAL_STATUS_DONE THEN			
				memset(ADR(SerialComm.Parameter.WriteData),0,SIZEOF(SerialComm.Parameter.WriteData)); //Clear write data
				SerialComm.Status.Step   := SERIAL_IDLE; 
			ELSIF SerialComm.FB.SerialComm.Status = IASERIAL_STATUS_ERROR THEN
				memset(ADR(SerialComm.Parameter.WriteData),0,SIZEOF(SerialComm.Parameter.WriteData)); //Clear write data
				SerialComm.Status.Step   := SERIAL_ERROR;
			END_IF
			
		SERIAL_ERROR: //254
			SerialComm.Status.Reading       := FALSE;
			SerialComm.Status.Writing       := FALSE;
			SerialComm.Status.Error         := TRUE;
			SerialComm.FB.SerialComm.Enable := FALSE;		
			SerialComm.Status.Step          := SERIAL_ERROR_RESET;
		
		SERIAL_ERROR_RESET:	//255	
			IF SerialComm.Command.AckError THEN
				SerialComm.Command.AckError     := FALSE;
				SerialComm.FB.ReadTimeout.IN    := FALSE;
				SerialComm.FB.SerialComm.Enable := TRUE;
				SerialComm.Status.Error         := FALSE;
				SerialComm.Status.Step          := SERIAL_IDLE;	
			ELSE
				SerialComm.FB.SerialComm.Enable := FALSE;
				SerialComm.Status.Error         := TRUE;
			END_IF
		
	END_CASE
	
	
	//Call FUBs
	SerialComm.FB.SerialComm();
	SerialComm.FB.ReadTimeout();
	
	//Reset cyclically so it doest stay true if not applicable
	SerialComm.Command.AckError := FALSE;
	
END_ACTION
