(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Program: serialmgr
 * File: serialmgr.typ
 * Author: mhanson
 * Created: June 15, 2015
 ********************************************************************
 * Local data types of program serialmgr
 ********************************************************************)
(*Serial Communication Structures*)

TYPE
	SerialComm_typ : 	STRUCT 
		Command : SerialComm_Command_typ;
		Status : SerialComm_Status_typ;
		Parameter : SerialComm_Parameter_typ;
		FB : SerialComm_FB_type;
	END_STRUCT;
	SerialComm_Command_typ : 	STRUCT 
		Read : BOOL;
		Write : BOOL;
		AckError : BOOL;
	END_STRUCT;
	SerialComm_Status_typ : 	STRUCT 
		ReadData : STRING[255];
		Step : USINT;
		NoData : BOOL; (*Indicates there is no data to read*)
		Writing : BOOL;
		Reading : BOOL;
		Error : BOOL;
		DataValid : BOOL;
	END_STRUCT;
	SerialComm_Parameter_typ : 	STRUCT 
		WriteData : STRING[255];
		Config : iASerial_Config_typ;
		ReadTimeout : TIME;
	END_STRUCT;
	SerialComm_FB_type : 	STRUCT 
		ReadTimeout : TON; (*Maximum amount of time between successful reads*)
		SerialComm : iASerialCyclic;
	END_STRUCT;
END_TYPE
