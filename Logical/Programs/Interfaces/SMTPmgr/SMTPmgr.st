(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: SMTPmgr
 * File: SMTPmgr.st
 * Author: phouston
 * Created:  July 07, 2016 11:34 AM
 * Version: 1.00.0
 ********************************************************************
 * Description:
 * 	Task to control the sendign of status emails from the PLC 
 * 	using the ASSMTP Library. 
 *
 * For more information about setting up and modifying the task see the 
 * "SMTP/Email Setup" section of the README.txt in Documetation
 *
 * Interfacing: This task interfaces with the hmimgr and alarmmgr tasks.
 ********************************************************************)

PROGRAM _CYCLIC

	IF (DataMgrInit) THEN
		// Check Network Connection
		IF (NetworkConnection) THEN	
			
			// Set Machine Running Bit if the machine is in AutoMode. 
			IF Mode = MODE_AUTO THEN
				MachineRunning := TRUE;
			ELSE
				MachineRunning := FALSE;
			END_IF
							
			//// Note on how the case statment is setup /////
			//// There is a setup case for each message type even though several messages have similar setup processes
			//// This is done for readability and so that if some additional information like alarms or statistics needs to be added
			//// to a specific message it is easy to the append the message text by adding to that state. 
			//// This does add length to the code but makes it easier to adapt for a specific project if the need arises. 
			//// As it is currently set up the PowerOn, Maint, Stopped, and Downtime are the same as are Alarm Critical and Fatal
			//// Once the message is setup it progresses to a Sending state then to a Sent state that it is in for a set time then moves back to wait
			
			CASE smtpState OF
				// Wait State
				WAIT:
					IF EmailCMD.PowerUpReminder THEN
						smtpState := POWERONMSG;
					ELSIF EmailCMD.StoppedReminder THEN
						smtpState := STOPPEDMSG;
					ELSIF EmailCMD.MachineStopReminder THEN
						smtpState := DOWNTIMEMSG;
					ELSIF EmailCMD.AlarmCritReminder THEN
						smtpState := CRITMSG;
					ELSIF EmailCMD.AlarmFatalReminder THEN
						smtpState := FATALMSG;
					ELSIF EmailCMD.CustomEmail THEN
						smtpState := CUSTOMMSG;
					ELSIF EmailCMD.MaintReminder THEN
						smtpState := MAINTMSG;
					END_IF
					HMI.SMTP.Status.StatusIndex := 1;
						
				// Send Maintenece Reminder Email
				MAINTMSG:	
					EmailCMD.MaintReminder := FALSE;
					
					IF SystemSettings.SMTP.MessageData[MAINT].Enable THEN
						// Assign Email Subject
						strcpy(ADR(Subject),ADR(SystemSettings.SMTP.ModelName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MachineName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MessageData[MAINT].Subject));
						// Assign Email Text
						strcpy(ADR(Text),ADR(SystemSettings.SMTP.MessageData[MAINT].Text));
						// Add Current Machine Runtime
						strcat(ADR(Text),ADR('$n $n The current runtime since last maintenance is:  '));
						strcat(ADR(Text),ADR(RunTime));
						strcat(ADR(Text),ADR('  hours'));
						// Add Current Recipe
						strcat(ADR(Text),ADR('$n $n The current recipe is: '));
						strcat(ADR(Text),ADR(Recipe.Name));
						// Add Current Build info String
						strcat(ADR(Text),ADR('$n $n The current build is: '));
						strcat(ADR(Text),ADR(G_BUILDINFO));					
						
						smtpState				:= SENDING;
					ELSE 
						smtpState				:= WAIT;
					END_IF
			
				// Send Email on Power Up
				POWERONMSG: 
					EmailCMD.PowerUpReminder 	:= FALSE;
				
					IF SystemSettings.SMTP.MessageData[POWERON].Enable THEN
						// Assign Email Subject
						strcpy(ADR(Subject),ADR(SystemSettings.SMTP.ModelName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MachineName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MessageData[POWERON].Subject));
						// Assign Email Text
						strcpy(ADR(Text),ADR(SystemSettings.SMTP.MessageData[POWERON].Text));
						// Add Current Machine Runtime
						strcat(ADR(Text),ADR('$n $n The current runtime since last maintenance is:  '));
						strcat(ADR(Text),ADR(RunTime));
						strcat(ADR(Text),ADR('  hours'));
						// Add Current Recipe
						strcat(ADR(Text),ADR('$n $n The current recipe is: '));
						strcat(ADR(Text),ADR(Recipe.Name));
						// Add Current Build info String
						strcat(ADR(Text),ADR('$n $n The current build is: '));
						strcat(ADR(Text),ADR(G_BUILDINFO));	
					
						smtpState				:= SENDING;
					ELSE
						smtpState				:= WAIT;
					END_IF
				
				// Send Email when the machine leaves automatic mode
				STOPPEDMSG: 
					EmailCMD.StoppedReminder	:= FALSE;
				
					IF SystemSettings.SMTP.MessageData[STOPPED].Enable THEN
						// Assign Email Subject
						strcpy(ADR(Subject),ADR(SystemSettings.SMTP.ModelName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MachineName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MessageData[STOPPED].Subject));
						// Assign Email Text
						strcpy(ADR(Text),ADR(SystemSettings.SMTP.MessageData[STOPPED].Text));
						// Add Current Machine Runtime
						strcat(ADR(Text),ADR('$n $n The current runtime since last maintenance is:  '));
						strcat(ADR(Text),ADR(RunTime));
						strcat(ADR(Text),ADR('  hours'));
						// Add Current Recipe
						strcat(ADR(Text),ADR('$n $n The current recipe is: '));
						strcat(ADR(Text),ADR(Recipe.Name));
						// Add Current Build info String
						strcat(ADR(Text),ADR('$n $n The current build is: '));
						strcat(ADR(Text),ADR(G_BUILDINFO));	
					
						smtpState				:= SENDING;
					ELSE
						smtpState				:= WAIT;
					END_IF
				
				// Send Email when machine stops for x amount of time
				DOWNTIMEMSG: 
					EmailCMD.MachineStopReminder := FALSE;
				
					IF SystemSettings.SMTP.MessageData[DOWN].Enable THEN
						// Assign Email Subject
						strcpy(ADR(Subject),ADR(SystemSettings.SMTP.ModelName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MachineName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MessageData[DOWN].Subject));
						// Assign Email Text
						strcpy(ADR(Text),ADR(SystemSettings.SMTP.MessageData[DOWN].Text));
						// Add Current Machine Runtime
						strcat(ADR(Text),ADR('$n $n The current runtime since last maintenance is:  '));
						strcat(ADR(Text),ADR(RunTime));
						strcat(ADR(Text),ADR('  hours'));
						// Add Current Recipe
						strcat(ADR(Text),ADR('$n $n The current recipe is: '));
						strcat(ADR(Text),ADR(Recipe.Name));
						// Add Current Build info String
						strcat(ADR(Text),ADR('$n $n The current build is: '));
						strcat(ADR(Text),ADR(G_BUILDINFO));	
						
						smtpState				:= SENDING;
					ELSE
						smtpState				:= WAIT;
					END_IF
			
				// Send Email for critical alarms
				CRITMSG:
					EmailCMD.AlarmCritReminder := FALSE;
				
					IF SystemSettings.SMTP.MessageData[ALARMCRIT].Enable THEN
						// Assign Email Subject
						strcpy(ADR(Subject),ADR(SystemSettings.SMTP.ModelName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MachineName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MessageData[ALARMCRIT].Subject));
						// Assign Email Text
						strcpy(ADR(Text),ADR(SystemSettings.SMTP.MessageData[ALARMCRIT].Text));
						// Assign Active Alarms Text
						strcat(ADR(Text),ADR('$n $n'));
						strcat(ADR(Text),ADR('The current active alarms are:'));
						strcat(ADR(Text),ADR('$n $n'));
						FOR i := 0 TO Alarm.Count DO
							strcat(ADR(Text),ADR(Alarm.Text[i]));
							strcat(ADR(Text),ADR(' $n '));
						END_FOR
						// Add Current Machine Runtime
						strcat(ADR(Text),ADR('$n $n The current runtime since last maintenance is:  '));
						strcat(ADR(Text),ADR(RunTime));
						strcat(ADR(Text),ADR('  hours'));
						// Add Current Recipe
						strcat(ADR(Text),ADR('$n $n The current recipe is: '));
						strcat(ADR(Text),ADR(Recipe.Name));
						// Add Current Build info String
						strcat(ADR(Text),ADR('$n $n The current build is: '));
						strcat(ADR(Text),ADR(G_BUILDINFO));	
						
						smtpState				:= SENDING;
					ELSE
						smtpState				:= WAIT;
					END_IF
					
				// Send Email for Fatal alarms
				FATALMSG:
					EmailCMD.AlarmFatalReminder := FALSE;
				
					IF SystemSettings.SMTP.MessageData[ALARMFATAL].Enable THEN
						// Assign Email Subject
						strcpy(ADR(Subject),ADR(SystemSettings.SMTP.ModelName));
						strcat(ADR(Subject),ADR(' '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MachineName));
						strcat(ADR(Subject),ADR('  '));
						strcat(ADR(Subject),ADR(SystemSettings.SMTP.MessageData[ALARMFATAL].Subject));
						// Assign Email Text
						strcpy(ADR(Text),ADR(SystemSettings.SMTP.MessageData[ALARMFATAL].Text));
						// Assign Active Alarms Text
						strcat(ADR(Text),ADR('$n $n'));
						strcat(ADR(Text),ADR('The current active alarms are:'));
						strcat(ADR(Text),ADR('$n $n'));
						FOR i := 0 TO Alarm.Count DO
							strcat(ADR(Text),ADR(Alarm.Text[i]));
							strcat(ADR(Text),ADR(' $n '));
						END_FOR
						// Add Current Machine Runtime
						strcat(ADR(Text),ADR('$n $n The current runtime since last maintenance is:  '));
						strcat(ADR(Text),ADR(RunTime));
						strcat(ADR(Text),ADR('  hours'));
						// Add Current Recipe
						strcat(ADR(Text),ADR('$n $n The current recipe is: '));
						strcat(ADR(Text),ADR(Recipe.Name));
						// Add Current Build info String
						strcat(ADR(Text),ADR('$n $n The current build is: '));
						strcat(ADR(Text),ADR(G_BUILDINFO));	
						
						smtpState				:= SENDING;
					ELSE
						smtpState				:= WAIT;
					END_IF
				
				// Test Email State
				CUSTOMMSG: 
					EmailCMD.CustomEmail := FALSE; 
									
					// Custom Email is built with out header and machine name and model like other emails
					strcpy(ADR(Subject),ADR(SystemSettings.SMTP.MessageData[5].Subject));
					strcpy(ADR(Text),ADR(SystemSettings.SMTP.MessageData[5].Text));
				
					smtpState				:= SENDING;
	
				// Wait for email to send state
				SENDING: 
					HMI.SMTP.Status.StatusIndex := 0;
					FB.SentTimer.IN := FALSE;
					
					// Setup recipients from HMI input
					IF SIZEOF(SystemSettings.SMTP.RecipientAddresses[0]) > 0 THEN
						NumberOfRecipients := SIZEOF(SystemSettings.SMTP.RecipientAddresses)/SIZEOF(SystemSettings.SMTP.RecipientAddresses[0]);
					END_IF
			
					brsstrcpy(ADR(Recipients),ADR(SystemSettings.SMTP.RecipientAddresses[0]));
					FOR i := 1 TO NumberOfRecipients-1 BY 1 DO
						result := strcmp(ADR(SystemSettings.SMTP.RecipientAddresses[i]),ADR(''));
						IF result <> 0 THEN
							brsstrcat(ADR(Recipients),ADR(','));
							brsstrcat(ADR(Recipients),ADR(SystemSettings.SMTP.RecipientAddresses[i]));
						END_IF
					END_FOR
					
					// Build IP String From array of USINTS
					FB.IP2Str(pIPString := ADR(HostIP), Octet1 := SystemSettings.SMTP.IPParts[0], Octet2 := SystemSettings.SMTP.IPParts[1], 
					Octet3 := SystemSettings.SMTP.IPParts[2], Octet4 := SystemSettings.SMTP.IPParts[3]);
					
					FB.SmtpSend.enable		:= TRUE;
					FB.SmtpSend.pReceiver 	:= ADR(Recipients);
					FB.SmtpSend.pSubject	:= ADR(Subject);
					FB.SmtpSend.pText		:= ADR(Text);
					FB.SmtpSend.pHost		:= ADR(HostIP);
					FB.SmtpSend.pUser		:= ADR(SystemSettings.SMTP.Username);
					FB.SmtpSend.pPassword	:= ADR(SystemSettings.SMTP.Password);
					FB.SmtpSend.pSender		:= ADR(SystemSettings.SMTP.SenderAdr);
					FB.SmtpSend.port		:= 465;
					FB.SmtpSend.pDomain		:= 0;
					FB.SmtpSend.pAttachment	:= 0;
					FB.SmtpSend.sslCfgIdent	:= 0;
					
					IF (FB.SmtpSend.status = ERR_OK) THEN
						FB.SmtpSend.enable	:= FALSE;
						smtpState := SENT;
					ELSIF (FB.SmtpSend.status <> ERR_FUB_BUSY) AND (FB.SmtpSend.status <> ERR_FUB_ENABLE_FALSE) THEN
						FB.SmtpSend.enable	:= FALSE;
						smtpState := ERROR;
						ErrorNumber := FB.SmtpSend.status;
					END_IF
	
				// Sent State
				SENT:
					HMI.SMTP.Status.StatusIndex := 3;
					FB.SentTimer.PT := T#2s;
					FB.SentTimer.IN	:= TRUE;
					
					IF FB.SentTimer.Q THEN
						smtpState := WAIT;
						FB.SentTimer.IN := FALSE;
					END_IF
				
				// Error Set State
				ERROR: 
					HMI.SMTP.Status.StatusIndex := 2;
					IF ErrorNumber = smtpERR_SOCKET_CONNECT THEN
						SMTP.Error[2] := TRUE;
					ELSIF ErrorNumber = smtpERR_RESPONSE_UNEXPECTED OR ErrorNumber = smtpERR_AUTHENTICATION_FAILED  THEN
						SMTP.Error[1] := TRUE;
					ELSE
						SMTP.Error[0] := TRUE;
					END_IF
										
					IF HMI.ResetPB OR EmailCMD.CustomEmail THEN
						// Error state is left when the global " Reset Faults button is set
						// or when another attempt to send a test/setup email is made
						smtpState := WAIT;
						FOR i := 0 TO SMTPERRORS DO
							SMTP.Error[i] := 0;
						END_FOR
					END_IF
			END_CASE;				
					
			// Timer For Machine Runtime
			IF EDGEPOS(DataMgrInit) THEN
				FB.MachineTimer.PT := SystemSettings.SMTP.SetInterval - SystemSettings.SMTP.ElapsedRuntime;
				OldMaintInterval := SystemSettings.SMTP.MessageData[MAINT].SendInterval;
			ELSIF EmailCMD.ResetInterval THEN
				FB.MachineTimer.PT := SystemSettings.SMTP.MessageData[MAINT].SendInterval;
				FB.MachineTimer.IN := FALSE; 
				SystemSettings.SMTP.TimersElapsed := 0;
				EmailCMD.ResetInterval := FALSE;
			ELSIF EDGENEG(MachineRunning) AND FB.MachineTimer.Q = FALSE THEN
				FB.MachineTimer.PT := FB.MachineTimer.PT - FB.MachineTimer.ET; // IF the machine is stopped before interval is up subtract runtime from set time
				FB.MachineTimer.IN := FALSE;
				SystemSettings.SMTP.SetInterval := FB.MachineTimer.PT; 
				SystemSettings.SMTP.ElapsedRuntime := FB.MachineTimer.ET;
			ELSIF EDGENEG(MachineRunning) AND FB.MachineTimer.Q THEN
				FB.MachineTimer.PT := SystemSettings.SMTP.MessageData[MAINT].SendInterval;	//If the machine turns off and time has elapsed reset to original interval
				FB.MachineTimer.IN := FALSE;
				EmailCMD.MaintReminder := TRUE;
				SystemSettings.SMTP.SetInterval := FB.MachineTimer.PT; 
				SystemSettings.SMTP.ElapsedRuntime := FB.MachineTimer.ET;
			ELSIF MachineRunning THEN
				IF NOT FB.MachineTimer.Q THEN
					FB.MachineTimer.IN := TRUE;
				ELSIF FB.MachineTimer.Q	THEN
					EmailCMD.MaintReminder := TRUE;
					FB.MachineTimer.IN := FALSE;
					FB.MachineTimer.PT := SystemSettings.SMTP.MessageData[MAINT].FollowUp;
					SystemSettings.SMTP.TimersElapsed := SystemSettings.SMTP.TimersElapsed + 1;
				END_IF
				SystemSettings.SMTP.SetInterval := FB.MachineTimer.PT; 
				SystemSettings.SMTP.ElapsedRuntime := FB.MachineTimer.ET;
			END_IF
					
			IF SystemSettings.SMTP.MessageData[MAINT].SendInterval <> OldMaintInterval THEN
				FB.MachineTimer.PT := SystemSettings.SMTP.MessageData[MAINT].SendInterval;
			END_IF
			OldMaintInterval := SystemSettings.SMTP.MessageData[MAINT].SendInterval;
			
			// Determine the runtime of the machine
			IF SystemSettings.SMTP.TimersElapsed = 0 THEN
				ElapsedTime := SystemSettings.SMTP.MessageData[MAINT].SendInterval - FB.MachineTimer.PT + FB.MachineTimer.ET;
				// If completed timers is zero them then elapsed time equals the current timer runtime plus the difference in the send interval and the current timer. 
			ELSIF SystemSettings.SMTP.TimersElapsed > 0 THEN
				ElapsedTime := SystemSettings.SMTP.MessageData[MAINT].SendInterval + SystemSettings.SMTP.MessageData[MAINT].FollowUp*(SystemSettings.SMTP.TimersElapsed-1) + SystemSettings.SMTP.MessageData[MAINT].FollowUp - FB.MachineTimer.PT + FB.MachineTimer.ET;
				// if  completed timers is one or more then elapsed time equals [ SendInterval + Number of elapsed FollowUp intervals + Current Runtime Of FollowUp Interval ] 
			END_IF
			Hours := TIME_TO_UDINT(ElapsedTime)/3600000;
			RunTime := UDINT_TO_STRING(Hours);
			
			// Display Warning Message
			IF ((FB.MachineTimer.PT - FB.MachineTimer.ET) < SystemSettings.SMTP.MessageData[MAINT].WarningTime) AND SystemSettings.SMTP.MessageData[MAINT].EnableWarning THEN
				SMTP.Error[3] := TRUE;
			ELSE
				SMTP.Error[3] := FALSE;
			END_IF
			
			// Timer for machine stopped for x ammount of time
			IF EDGENEG(MachineRunning) THEN
				FB.StoppedTimer.IN := TRUE;
				FB.StoppedTimer.PT := SystemSettings.SMTP.MessageData[DOWN].SendInterval;
				EmailCMD.StoppedReminder := TRUE;
			ELSIF EDGEPOS(MachineRunning) THEN
				FB.StoppedTimer.IN := FALSE;
			ELSIF (MachineRunning = 0) AND FB.StoppedTimer.Q THEN
				FB.StoppedTimer.IN := FALSE;
				EmailCMD.MachineStopReminder := TRUE;
			END_IF
					
			// Use Visapi to get current alarm text for alarm emails 
			IF Ready = 0 THEN
				IF vcHandle <> 0 THEN
					Ready := 1;
				END_IF
			ELSE
				Access_Status := VA_Saccess(1,vcHandle);
				IF Access_Status = 0 THEN
					CASE VAstep OF 
						
						// Wait State
						WAIT: 
							IF SystemSettings.SMTP.MessageData[ALARMFATAL].Enable AND EDGEPOS(AlarmMgr.ErrFatal) THEN
								VAstep := GETALARMS; 
							ELSIF SystemSettings.SMTP.MessageData[ALARMCRIT].Enable AND EDGEPOS(AlarmMgr.ErrCritical) THEN
								VAstep := GETALARMS; 
							END_IF
						
						// Determine number of active alarms
						GETALARMS: 
							CountStatus := VA_GetActiveAlarmCount(1,vcHandle,ADR(Alarm.Count));
							
							IF CountStatus = 0 THEN
								VAstep := READFIRST;
							END_IF
						
						// Read first alarm
						READFIRST: 
							IF Alarm.Count > NUMBEROFALARMTEXTS THEN
								Alarm.Count := NUMBEROFALARMTEXTS;
							END_IF
							
							Alarm.AlarmLen := 150;
							Alarm.Sparator := 32;
							Alarm.TimeFormat := 6;
							Alarm.Type := 1;
							
							ActStatus := VA_GetActAlarmList(1,vcHandle,ADR(Alarm.Text[0]),ADR(Alarm.AlarmLen),Alarm.Type,Alarm.Sparator,Alarm.TimeFormat);
							IF ActStatus = 0 THEN
								VAstep := READNEXT;
							END_IF
						
						// Read next alarm until the alarm count is reached
						READNEXT:
							i := 1; 
							WHILE i < Alarm.Count DO
								Alarm.AlarmLen := 150;
								Alarm.Sparator := 32;
								Alarm.TimeFormat := 6;
								Alarm.Type := 2;
								
								ActStatus := VA_GetActAlarmList(1,vcHandle,ADR(Alarm.Text[i]),ADR(Alarm.AlarmLen),Alarm.Type,Alarm.Sparator,Alarm.TimeFormat);
								
								IF ActStatus = 0 THEN
									i := i + 1;
								END_IF
							END_WHILE
							
							IF AlarmMgr.ErrFatal THEN
								EmailCMD.AlarmFatalReminder := TRUE;
							ELSIF AlarmMgr.ErrCritical THEN
								EmailCMD.AlarmCritReminder := TRUE;
							END_IF
							VAstep := WAIT;
					END_CASE
				END_IF
				VA_Srelease(1,vcHandle);
			END_IF
			
			FB.StoppedTimer();
			FB.SentTimer();
			FB.MachineTimer();
			FB.SmtpSend();	
		END_IF
		
		// Test Network Connection
		CASE IcmpState OF
			WAIT:
				FB.IcmpTimer.IN := TRUE;
				
				IF FB.IcmpTimer.Q THEN
					cmdPing := TRUE;
				END_IF
								
				IF cmdPing THEN
					IcmpState := CHECKCONNECTION;
					FB.IcmpTimer.IN := FALSE;
				END_IF
			
			CHECKCONNECTION:
				FB.IcmpPing.enable 	:= TRUE;
				cmdPing := FALSE;
				
				IF FB.IcmpPing.status = 0 THEN  
					IcmpState := WAIT;
					NetworkConnection := TRUE;
					FB.IcmpPing.enable 	:= FALSE;
					IcmpError := 0;
				ELSIF FB.IcmpPing.status = ERR_FUB_BUSY OR FB.IcmpPing.status = ERR_FUB_ENABLE_FALSE THEN 	
					IcmpState := CHECKCONNECTION;
				ELSE 
					NetworkConnection := FALSE;
					FB.IcmpPing.enable 	:= FALSE;
					IcmpState := WAIT;
					IcmpError := FB.IcmpPing.status;
				END_IF
			
				IF NetworkConnection = FALSE THEN
					SMTP.Error[4] := TRUE;
				ELSE
					SMTP.Error[4] := FALSE;
				END_IF
		END_CASE;
				
		FB.IcmpPing();
		FB.IcmpTimer();
	END_IF	
	
END_PROGRAM

PROGRAM _INIT
	
	FB.SmtpSend.port	:= 465;
	EmailCMD.PowerUpReminder := TRUE;

	VAstep		:= WAIT;
	smtpState	:= WAIT;
	
	cmdPing				:= TRUE;
	PingIP				:= '8.8.8.8';	// Ping DNS server to test network connection
	FB.IcmpPing.pHost 	:= ADR(PingIP); 
	FB.IcmpPing.timeout	:= 50; // 50 milliseconds
	FB.IcmpTimer.PT		:= T#5m;
	
	// Request Email On StartUp
	EmailCMD.PowerUpReminder := TRUE;
	
END_PROGRAM