
TYPE
	FB_typ : 	STRUCT 
		SmtpSend : SmtpsSend;
		StoppedTimer : TON;
		MachineTimer : TON;
		SentTimer : TON;
		IP2Str : IP2Str;
		IcmpTimer : TON;
		IcmpPing : IcmpPing;
	END_STRUCT;
	SmtpCMD_Typ : 	STRUCT 
		MachineStopReminder : BOOL; (*cmd to send machine stop email*)
		PowerUpReminder : BOOL; (*cmd to Send email wehn machine powers on*)
		MaintReminder : BOOL; (*cmd to send maitenence reminder email*)
		ResetInterval : BOOL;
		AlarmFatalReminder : BOOL;
		AlarmCritReminder : BOOL;
		CustomEmail : BOOL;
		StoppedReminder : BOOL; (*cmd to send maitenence reminder email*)
	END_STRUCT;
	AlarmInfo_Typ : 	STRUCT 
		iFunction : UINT;
		AlarmLen : DINT;
		Sparator : USINT;
		Text : ARRAY[0..NUMBEROFALARMTEXTS]OF STRING[150];
		TimeFormat : USINT;
		Type : USINT;
		Count : UINT;
	END_STRUCT;
	State_Enum : 
		(
		CUSTOMMSG := 40,
		DOWNTIMEMSG := 30,
		FATALMSG := 50,
		CRITMSG := 60,
		POWERONMSG := 20,
		WAIT := 0,
		MAINTMSG := 10,
		ERROR := 255,
		SENT := 250,
		GETALARMS := 5,
		SENDING := 240,
		READNEXT := 25,
		READFIRST := 15,
		CHECKCONNECTION := 100,
		STOPPEDMSG := 70
		);
END_TYPE
