(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: tempmgr
 * File: tempmgr.st
 * Author: ?
 * Created: long ago
 * Version: 1.00.0
 * Modified Date: 06/10/2015
 * Modified By: pholleger
 ********************************************************************
 * Implementation OF PROGRAM tempmgr

 * Description:

	This program manages temperature control of different temperature
	zones, ranging from zone 0..MaxZones. PID control and autotune
	are both handled within this task.

 * Options:

 * Version History:
 ********************************************************************)
PROGRAM _CYCLIC

	IF (DataMgrInit) THEN

		FOR ZoneIndex := 0 TO (MaxZones - 1) DO
			// X20 documentation says these are the lowest AND highest values we can see, everything ELSE is error
			IF ( (AI_Temperatures[ZoneIndex] < -2000) OR (AI_Temperatures[ZoneIndex] > 8500) ) THEN
				TempMgr.data.ActualTemp[ZoneIndex] := 0;
				TempMgr.data.NotDetected[ZoneIndex] := 1;
				IF (TempMgr.State[ZoneIndex] <> STATE_ERROR) THEN
					TempMgr.State[ZoneIndex] := STATE_ERROR;
				END_IF
			ELSE
				TempMgr.data.ActualTemp[ZoneIndex] := AI_Temperatures[ZoneIndex] / 10.0;
				TempMgr.data.NotDetected[ZoneIndex] := 0;
			END_IF
	
			TempTune[ZoneIndex].Temp_set := Recipe.TemperatureZone[ZoneIndex].SetPoint;
			TempTune[ZoneIndex].Temp := TempMgr.data.ActualTemp[ZoneIndex];
		
			TempController[ZoneIndex].Temp_set := Recipe.TemperatureZone[ZoneIndex].SetPoint;
			TempController[ZoneIndex].Temp := TempMgr.data.ActualTemp[ZoneIndex];
		
			TempMgr.data.InRange[ZoneIndex] :=  (TempMgr.data.ActualTemp[ZoneIndex] >= (Recipe.TemperatureZone[ZoneIndex].SetPoint - Recipe.TemperatureZone[ZoneIndex].ToleranceLow)) AND (TempMgr.data.ActualTemp[ZoneIndex] <= (Recipe.TemperatureZone[ZoneIndex].SetPoint + Recipe.TemperatureZone[ZoneIndex].ToleranceHigh));
		
		
			CASE TempMgr.State[ZoneIndex] OF 
			
				STATE_OFF:
					TempTune[ZoneIndex].okToHeat := 0;
					TempTune[ZoneIndex].enable := 0;
					TempTune[ZoneIndex].start := 0;
					TempController[ZoneIndex].enable := 0;
									
					IF (TempMgr.cmd.AutoTune[ZoneIndex]) THEN
						IF ( (TempMgr.data.NotDetected[ZoneIndex]) OR (NOT Recipe.TemperatureZone[ZoneIndex].Enabled) OR (SystemSettings.TemperatureLimit <= 0) OR (SystemSettings.TemperatureLimit > 400) ) THEN
							TempMgr.cmd.AutoTune[ZoneIndex] := 0;
						ELSE

							TempTune[ZoneIndex].okToHeat := 1;
							TempTune[ZoneIndex].enable := 1;
							TempTune[ZoneIndex].start := 1;
							TempMgr.State[ZoneIndex] := STATE_AUTOTUNE;
						END_IF
				
					ELSIF (TempMgr.cmd.On[ZoneIndex]) THEN
						IF (AlarmMgr.ErrFatal = TRUE) THEN
							TempMgr.cmd.On[ZoneIndex] := 0;
						ELSE				
							TempController[ZoneIndex].mode := LCRTEMPPID_MODE_AUTO;
							TempController[ZoneIndex].enable := 1;
							TempMgr.State[ZoneIndex] := STATE_ON;
						END_IF
					
					END_IF
			
			
				STATE_ON:
					
					TempMgr.cmd.AutoTune[ZoneIndex] := 0;
				
					IF ( (NOT TempMgr.cmd.On[ZoneIndex]) OR (NOT Recipe.TemperatureZone[ZoneIndex].Enabled) ) THEN
						TempController[ZoneIndex].enable := 0;
						TempMgr.State[ZoneIndex] := STATE_OFF;

					ELSIF (TempMgr.cmd.AutoTune[ZoneIndex]) THEN
						IF ( (TempMgr.data.NotDetected[ZoneIndex]) OR (NOT Recipe.TemperatureZone[ZoneIndex].Enabled) OR (SystemSettings.TemperatureLimit <= 0) OR (SystemSettings.TemperatureLimit > 400) ) THEN
							TempMgr.cmd.AutoTune[ZoneIndex] := 0;
						ELSE
							TempController[ZoneIndex].enable := 0;
							TempTune[ZoneIndex].okToHeat := 1;
							TempTune[ZoneIndex].enable := 1;
							TempTune[ZoneIndex].start := 1;
							TempMgr.State[ZoneIndex] := STATE_AUTOTUNE;
						END_IF


					END_IF
				
			
			
				STATE_AUTOTUNE:
				
					TempMgr.cmd.On[ZoneIndex] := 0;
				
					IF ( (NOT TempMgr.cmd.AutoTune[ZoneIndex]) OR (NOT Recipe.TemperatureZone[ZoneIndex].Enabled) ) THEN
						TempTune[ZoneIndex].enable := 0;
						TempTune[ZoneIndex].start := 0;
					
						TempMgr.data.AutoTuning[ZoneIndex] := 0;
						TempMgr.State[ZoneIndex] := STATE_OFF;
					
					ELSIF (TempTune[ZoneIndex].done) THEN
						TempTune[ZoneIndex].enable := 0;
						TempTune[ZoneIndex].start := 0;
						TempTune[ZoneIndex].okToHeat := 0;
					
						TempMgr.cmd.AutoTune[ZoneIndex] := 0;
						TempMgr.State[ZoneIndex] := STATE_OFF;
				
					END_IF
				
			
			
				STATE_ERROR:
					TempMgr.cmd.AutoTune[ZoneIndex] := 0;
					TempMgr.cmd.On[ZoneIndex] := 0;
					TempTune[ZoneIndex].enable := 0;
					TempTune[ZoneIndex].start := 0;
					TempController[ZoneIndex].enable := 0;
					TempTune[ZoneIndex].okToHeat := 0; 
	
					IF (NOT TempMgr.data.NotDetected[ZoneIndex]) THEN
						TempMgr.State[ZoneIndex] := STATE_OFF;
					END_IF
	
			
			
			END_CASE

	
		END_FOR

		// prevent fires
		IF ( (TempMgr.data.ActualTemp[0] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[1] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[2] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[3] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[4] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[5] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[6] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[7] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[8] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[9] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[10] >= SystemSettings.TemperatureLimit) OR 
			 (TempMgr.data.ActualTemp[11] >= SystemSettings.TemperatureLimit) ) THEN
			
			TempMgr.data.LimitExceeded := 1;

			FOR i := 0 TO (MaxZones -1) DO
	 
				TempMgr.cmd.On[i] := 0;
				TempMgr.cmd.AutoTune[i] := 0;

			END_FOR;
		ELSE 
			TempMgr.data.LimitExceeded := 0;
		END_IF


		TempTune[0]();
		TempTune[1]();
		TempTune[2]();
		TempTune[3]();
		TempTune[4]();
		TempTune[5]();
		TempTune[6]();
		TempTune[7]();
		TempTune[8]();
		TempTune[9]();
		TempTune[10]();
		TempTune[11]();

		FOR ZoneIndex := 0 TO (MaxZones - 1) DO
		
			TempMgr.data.AutoTuning[ZoneIndex] := TempTune[ZoneIndex].busy;
		
		END_FOR


		TempController[0]();
		TempController[1]();
		TempController[2]();
		TempController[3]();
		TempController[4]();
		TempController[5]();
		TempController[6]();
		TempController[7]();
		TempController[8]();
		TempController[9]();
		TempController[10]();
		TempController[11]();
		

		FOR ZoneIndex := 0 TO (MaxZones - 1) DO
	
			TempMgr.data.On[ZoneIndex] := TempController[ZoneIndex].enable;
		
		END_FOR

		FOR ZoneIndex := 0 TO (MaxZones - 1) DO
		
			TempOutput[ZoneIndex].enable := TempMgr.data.On[ZoneIndex] OR TempMgr.data.AutoTuning[ZoneIndex];
		
			IF (TempMgr.data.AutoTuning[ZoneIndex]) THEN
				TempOutput[ZoneIndex].x := TempTune[ZoneIndex].y_heat;
			ELSE
				TempOutput[ZoneIndex].x := TempController[ZoneIndex].y_heat;
			END_IF
	
		END_FOR

		TempOutput[0]();
		TempOutput[1]();
		TempOutput[2]();
		TempOutput[3]();
		TempOutput[4]();
		TempOutput[5]();
		TempOutput[6]();
		TempOutput[7]();
		TempOutput[8]();
		TempOutput[9]();
		TempOutput[10]();
		TempOutput[11]();

				
	END_IF
	
END_PROGRAM



PROGRAM _INIT

	FOR ZoneIndex := 0 TO (MaxZones - 1) DO
		
		SystemSettings.TemperatureZone[ZoneIndex].ControllerSettings.TuneSet.mode := LCRTEMPTune_MODE_DEF;
		SystemSettings.TemperatureZone[ZoneIndex].ControllerSettings.TuneSet.filter_base_T := 2;

		TempTune[ZoneIndex].pSettings		:= ADR(SystemSettings.TemperatureZone[ZoneIndex].ControllerSettings);
		TempController[ZoneIndex].pSettings := ADR(SystemSettings.TemperatureZone[ZoneIndex].ControllerSettings);
		
		TempOutput[ZoneIndex].max_value 	:= 100;
		TempOutput[ZoneIndex].min_value 	:= 0;
		TempOutput[ZoneIndex].t_min_pulse 	:= 0.1;
		TempOutput[ZoneIndex].t_period 		:= TempOutput[ZoneIndex].t_min_pulse;
		
		//Disable cooling and enable heating
		SystemSettings.TemperatureZone[ZoneIndex].ControllerSettings.disable_heating	:= FALSE;
		SystemSettings.TemperatureZone[ZoneIndex].ControllerSettings.enable_cooling		:= FALSE;

	
	END_FOR
	
END_PROGRAM


PROGRAM _EXIT

	//Force all DOs to OFF
//	FOR ZoneIndex := 0 TO (MaxZones - 1) DO
//		
//		DO_Heater[ZoneIndex]	:= FALSE;
//		
//	END_FOR;
		
END_PROGRAM

