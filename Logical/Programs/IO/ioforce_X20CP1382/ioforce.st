
PROGRAM _CYCLIC

	IF (IOForce.cmd.DisableAllForcing) THEN
		IOForce.cmd.DisableAllForcing := 0;
		FOR i := 0 TO (N-1) DO
			IOForce.cmd.Enable[i] := 0;
		END_FOR
	END_IF
	
	IOForce.data.ForcingEnabled := 0;
	
	FOR i := 0 TO (N-1) DO
	
		CASE Channel[i].Step OF
			IO_NOT_FORCED:  // not forced;
				IF (IOForce.cmd.Enable[i]) THEN // IF enabled, first set the force value;
					Channel[i].FB.SetForceValue.value := IOForce.cmd.ForceValue[i];
					Channel[i].FB.SetForceValue.enable := 1;
					Channel[i].Step := 1;
				END_IF
			
		
			IO_WAIT_FORCE_VALUE: // wait for force value TO be set ;
				IF (Channel[i].FB.SetForceValue.status = 0) THEN
					Channel[i].FB.SetForceValue.enable := 0;
					Channel[i].FB.EnableForcing.enable := 1; // now enable forcing itself;
					Channel[i].Step := 2;
				END_IF
			
			
			IO_WAIT_FORCE_ENABLE:   // wait for forcing TO be enabled;
				IF (Channel[i].FB.EnableForcing.status = 0) THEN
					Channel[i].FB.EnableForcing.enable := 0;
					Channel[i].Step := 3;
				END_IF
			
			
			IO_FORCING_ENABLED:  // forcing is now enabled.;
				IF (NOT IOForce.cmd.Enable[i]) THEN
					Channel[i].FB.DisableForcing.enable := 1;
					Channel[i].Step := 5;
					
				ELSIF (IOForce.cmd.ForceValue[i] <> Channel[i].FB.SetForceValue.value) THEN
					Channel[i].FB.SetForceValue.value := IOForce.cmd.ForceValue[i];
					Channel[i].FB.SetForceValue.enable := 1;
					Channel[i].Step := 4;
				END_IF
			
			
			IO_CHANGE_FORCE_VALUE: // change force value;
				IF (Channel[i].FB.SetForceValue.status = 0) THEN
					Channel[i].FB.SetForceValue.enable := 0;			
					Channel[i].Step := 3;
				END_IF
			
			
			IO_DISABLE_FORCING:
				IF (Channel[i].FB.DisableForcing.status = 0) THEN
					Channel[i].FB.DisableForcing.enable := 0;
					Channel[i].Step := 0;
				END_IF
				
			
		END_CASE
	
		Channel[i].FB.EnableForcing();
		Channel[i].FB.DisableForcing();
		Channel[i].FB.SetForceValue();
		Channel[i].FB.DatapointStatus();
		
		IF (BIT_TST(Channel[i].FB.DatapointStatus.flags, 2) = 1) THEN // check IF forcing is enabled;
			IOForce.cmd.Enable[i] := 1;
			IOForce.data.ForcingEnabled := 1;
			CASE Channel[i].FB.DatapointStatus.datatype OF
				2: // SINT;
					tempsint ACCESS ADR(Channel[i].FB.DatapointStatus.forceValue);
					IOForce.data.PhysicalValue[i] := tempsint;
					IOForce.cmd.ForceValue[i] := tempsint;
				
				
				3: // INT;
					tempint ACCESS ADR(Channel[i].FB.DatapointStatus.forceValue);
					IOForce.data.PhysicalValue[i] := tempint;
					IOForce.cmd.ForceValue[i] := tempint;
				
				
				4: // DINT;
					tempdint ACCESS ADR(Channel[i].FB.DatapointStatus.forceValue);
					IOForce.data.PhysicalValue[i] := tempdint;
					IOForce.cmd.ForceValue[i] := tempdint;				
						
				ELSE
					IOForce.data.PhysicalValue[i] := Channel[i].FB.DatapointStatus.forceValue;
					IOForce.cmd.ForceValue[i] := Channel[i].FB.DatapointStatus.forceValue;		
				
			END_CASE
			
		ELSIF (BIT_TST(Channel[i].FB.DatapointStatus.flags, 1) = 1) THEN // check that the io point is valid;
			IOForce.cmd.Enable[i] := 0;
			
			CASE Channel[i].FB.DatapointStatus.datatype OF
				2: // SINT;
					tempsint ACCESS ADR(Channel[i].FB.DatapointStatus.value);
					IOForce.data.PhysicalValue[i] := tempsint;
					IOForce.cmd.ForceValue[i] := tempsint;
				
				
				3: // INT;
					tempint ACCESS ADR(Channel[i].FB.DatapointStatus.value);
					IOForce.data.PhysicalValue[i] := tempint;
					IOForce.cmd.ForceValue[i] := tempint;
				
				
				4: // DINT;
					tempdint ACCESS ADR(Channel[i].FB.DatapointStatus.value);
					IOForce.data.PhysicalValue[i] := tempdint;
					IOForce.cmd.ForceValue[i] := tempdint;
								
						
				ELSE
					IOForce.data.PhysicalValue[i] := Channel[i].FB.DatapointStatus.value;
					IOForce.cmd.ForceValue[i] := Channel[i].FB.DatapointStatus.value;		
				
			END_CASE
			
		ELSE
			IOForce.cmd.Enable[i] := 0;
			
			CASE Channel[i].FB.DatapointStatus.datatype OF
				2: // SINT;
					tempsint ACCESS ADR(Channel[i].FB.DatapointStatus.defaultValue);
					IOForce.data.PhysicalValue[i] := tempsint;
					IOForce.cmd.ForceValue[i] := tempsint;
				
				
				3: // INT;
					tempint ACCESS ADR(Channel[i].FB.DatapointStatus.defaultValue);
					IOForce.data.PhysicalValue[i] := tempint;
					IOForce.cmd.ForceValue[i] := tempint;
				
				
				4: // DINT;
					tempdint ACCESS ADR(Channel[i].FB.DatapointStatus.defaultValue);
					IOForce.data.PhysicalValue[i] := tempdint;
					IOForce.cmd.ForceValue[i] := tempdint;
								
						
				ELSE
					IOForce.data.PhysicalValue[i] := Channel[i].FB.DatapointStatus.defaultValue;
					IOForce.cmd.ForceValue[i] := Channel[i].FB.DatapointStatus.defaultValue;		
				
			END_CASE			
	
		END_IF
			
	END_FOR

END_PROGRAM



PROGRAM _INIT;
	
	N := SIZEOF(Channel) / SIZEOF(Channel[0]);
	
	IF ( ((SIZEOF(IOForce.cmd.Enable)/SIZEOF(IOForce.cmd.Enable[0])) <> N) OR
	     ((SIZEOF(IOForce.cmd.ForceValue)/SIZEOF(IOForce.cmd.ForceValue[0])) <> N) OR
	 	 ((SIZEOF(IOForce.data.PhysicalValue)/SIZEOF(IOForce.data.PhysicalValue[0])) <> N) ) THEN
		ERRxfatal(0,0,ADR('range of IOForce.cmd.Enable <> IOForce.cmd.ForceValue <> range of IOForce.data.PhysicalValue <> range of Channel'));
	END_IF
	
	// NOTE: YOU CAN EASILY GET THE CHANNEL NAMES BY MAPPING YOUR IO THEN OPENING THE IOMAP.IOM FILE AS TEXT!
	
	//X1
	strcpy(ADR(Channel[0].Datapoint), ADR('%IX.X1.DigitalInput01'));
	strcpy(ADR(Channel[1].Datapoint), ADR('%IX.X1.DigitalInput02'));
	strcpy(ADR(Channel[2].Datapoint), ADR('%IX.X1.DigitalInput03'));
	strcpy(ADR(Channel[3].Datapoint), ADR('%IX.X1.DigitalInput04'));
	
	//X2
	strcpy(ADR(Channel[4].Datapoint), ADR('%IX.X2.DigitalInput01'));
	strcpy(ADR(Channel[5].Datapoint), ADR('%IX.X2.DigitalInput02'));
	strcpy(ADR(Channel[6].Datapoint), ADR('%IX.X2.DigitalInput03'));
	strcpy(ADR(Channel[7].Datapoint), ADR('%IX.X2.DigitalInput04'));
	strcpy(ADR(Channel[8].Datapoint), ADR('%IX.X2.DigitalInput05'));
	strcpy(ADR(Channel[9].Datapoint), ADR('%IX.X2.DigitalInput06'));
	strcpy(ADR(Channel[10].Datapoint), ADR('%IX.X2.DigitalInput07'));
	strcpy(ADR(Channel[11].Datapoint), ADR('%IX.X2.DigitalInput08'));
	strcpy(ADR(Channel[12].Datapoint), ADR('%IX.X2.DigitalInput09'));
	strcpy(ADR(Channel[13].Datapoint), ADR('%IX.X2.DigitalInput10'));
	
	//X3
	strcpy(ADR(Channel[14].Datapoint), ADR('%QX.X3.DigitalOutput01'));
	strcpy(ADR(Channel[15].Datapoint), ADR('%QX.X3.DigitalOutput02'));
	strcpy(ADR(Channel[16].Datapoint), ADR('%QX.X3.DigitalOutput03'));
	strcpy(ADR(Channel[17].Datapoint), ADR('%QX.X3.DigitalOutput04'));
	strcpy(ADR(Channel[18].Datapoint), ADR('%QX.X3.DigitalOutput05'));
	strcpy(ADR(Channel[19].Datapoint), ADR('%QX.X3.DigitalOutput06'));
	strcpy(ADR(Channel[20].Datapoint), ADR('%QX.X3.DigitalOutput07'));
	strcpy(ADR(Channel[21].Datapoint), ADR('%QX.X3.DigitalOutput08'));
	strcpy(ADR(Channel[22].Datapoint), ADR('%QX.X3.DigitalOutput09'));
	strcpy(ADR(Channel[23].Datapoint), ADR('%QX.X3.DigitalOutput10'));
	strcpy(ADR(Channel[24].Datapoint), ADR('%QX.X3.DigitalOutput11'));
	strcpy(ADR(Channel[25].Datapoint), ADR('%QX.X3.DigitalOutput12'));

	// DI4371
	strcpy(ADR(Channel[26].Datapoint), ADR('%IX.IF6.ST1.DigitalInput01'));
	strcpy(ADR(Channel[27].Datapoint), ADR('%IX.IF6.ST1.DigitalInput02'));
	strcpy(ADR(Channel[28].Datapoint), ADR('%IX.IF6.ST1.DigitalInput03'));
	strcpy(ADR(Channel[29].Datapoint), ADR('%IX.IF6.ST1.DigitalInput04'));
	
	// DI6371
	strcpy(ADR(Channel[30].Datapoint), ADR('%IX.IF6.ST2.DigitalInput01'));
	strcpy(ADR(Channel[31].Datapoint), ADR('%IX.IF6.ST2.DigitalInput02'));
	strcpy(ADR(Channel[32].Datapoint), ADR('%IX.IF6.ST2.DigitalInput03'));
	strcpy(ADR(Channel[33].Datapoint), ADR('%IX.IF6.ST2.DigitalInput04'));
	strcpy(ADR(Channel[34].Datapoint), ADR('%IX.IF6.ST2.DigitalInput05'));
	strcpy(ADR(Channel[35].Datapoint), ADR('%IX.IF6.ST2.DigitalInput06'));	
	
	// DO4322;
	strcpy(ADR(Channel[36].Datapoint), ADR('%QX.IF6.ST3.DigitalOutput01'));
	strcpy(ADR(Channel[37].Datapoint), ADR('%QX.IF6.ST3.DigitalOutput02'));
	strcpy(ADR(Channel[38].Datapoint), ADR('%QX.IF6.ST3.DigitalOutput03'));
	strcpy(ADR(Channel[39].Datapoint), ADR('%QX.IF6.ST3.DigitalOutput04'));
	
	// DO6321
	strcpy(ADR(Channel[40].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput01'));
	strcpy(ADR(Channel[41].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput02'));
	strcpy(ADR(Channel[42].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput03'));
	strcpy(ADR(Channel[43].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput04'));
	strcpy(ADR(Channel[44].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput05'));
	strcpy(ADR(Channel[45].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput06'));	
	
	// DO9321
	strcpy(ADR(Channel[46].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput01'));
	strcpy(ADR(Channel[47].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput02'));
	strcpy(ADR(Channel[48].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput03'));
	strcpy(ADR(Channel[49].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput04'));
	strcpy(ADR(Channel[50].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput05'));
	strcpy(ADR(Channel[51].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput06'));
	strcpy(ADR(Channel[52].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput07'));
	strcpy(ADR(Channel[53].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput08'));
	strcpy(ADR(Channel[54].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput09'));
	strcpy(ADR(Channel[55].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput10'));
	strcpy(ADR(Channel[56].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput11'));
	strcpy(ADR(Channel[57].Datapoint), ADR('%QX.IF6.ST5.DigitalOutput12'));	


	FOR i := 0 TO (N-1) DO
		Channel[i].FB.EnableForcing.pDatapoint := ADR(Channel[i].Datapoint);
		Channel[i].FB.DisableForcing.pDatapoint := ADR(Channel[i].Datapoint);
		Channel[i].FB.SetForceValue.pDatapoint := ADR(Channel[i].Datapoint);
		Channel[i].FB.DatapointStatus.pDatapoint := ADR(Channel[i].Datapoint);
		Channel[i].FB.DatapointStatus.enable := 1;
	END_FOR


END_PROGRAM

